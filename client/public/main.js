const { app, BrowserWindow } = require("electron");
const isDev = require("electron-is-dev");

require("@electron/remote/main").initialize();

function createWindow() {
  let win = new BrowserWindow({
    width: 1440,
    height: 1024,
    minWidth: 700,
    minHeight: 550,
    icon: __dirname + '/favicon.png',
    webPreferences: {
      nodeIntegration: true,
      enebleRemoteModule: true,
      webSecurity: false,
    },
    autoHideMenuBar: true,
  });

  win.loadURL(
    isDev ? "http://localhost:3000" : `file://${__dirname}/../build/index.html`
  );

  win.on("closed", () => (win = null));
}

app.on("ready", createWindow);

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) createWindow();
});
