import ReactDOM from "react-dom/client";
import "./index.scss";
import App from "./App";
import { HashRouter } from "react-router-dom";
import { UserContextProvider } from "./contexts/user";
import "react-tooltip/dist/react-tooltip.css";
import { TranslationsProvider } from "./contexts/translations";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <HashRouter>
    <TranslationsProvider>
      <UserContextProvider>
        <App />
      </UserContextProvider>
    </TranslationsProvider>
  </HashRouter>
);
