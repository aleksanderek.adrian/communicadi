import React from "react";
import css from "./App.module.scss";
import Header from "./components/header/Header";

import { Routes, Route, Outlet, useLocation } from "react-router-dom";

import Login from "./screens/login/Login";
import Register from "./screens/register/Register";
import { useUserContext } from "./contexts/user";

import { SocketProvider, useSocket } from "./contexts/socket";
import ContactsScreen from "./screens/contacts/ContactsScreen";
import {
  ContactsContextProvider,
  useContactsContext,
} from "./contexts/contacts";
import {
  ConversationsContextProvider,
  useConversationContext,
} from "./contexts/conversations";
import ConversationsScreen from "./screens/conversations/ConversationsScreen";
import { LoadingModal } from "./components/loading/LoadingSpinner";
import {
  ModalContextProvider,
  ModalManager,
  useModalContext,
} from "./contexts/modal";

const Dashboard = () => {
  const conversationsContext = useConversationContext();
  const contactsContext = useContactsContext();
  const modalContext = useModalContext();
  const { connected } = useSocket();
  const [loading, setLoading] = React.useState(true);

  React.useEffect(() => {
    const init = async () => {
      try {
        setLoading(true);
        await contactsContext.fetchContacts();
        await conversationsContext.fetchConversations();
        await conversationsContext.fetchInbox();
      } catch {
        modalContext.openDataFetchErrorModal();
      }
      setLoading(false);
    };
    init();
    // eslint-disable-next-line
  }, [connected]);

  return (
    <div className={css.dashboard}>
      {loading && <LoadingModal />}
      <Header />
      <Outlet />
    </div>
  );
};

const App = () => {
  const userContext = useUserContext();

  const location = useLocation();
  const previousLocation = location.state?.previousLocation;

  let routes;

  if (userContext.token) {
    routes = (
      <Routes location={previousLocation || location}>
        <Route index element={<Login />} />
        <Route path="/register" element={<Register />} />

        <Route path="/dashboard" element={<Dashboard />}>
          <Route path="conversations/*" element={<ConversationsScreen />} />
          <Route path="contacts/*" element={<ContactsScreen />} />
        </Route>
      </Routes>
    );
  } else {
    routes = (
      <Routes>
        <Route index element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="*" element={<Login />} />
      </Routes>
    );
  }

  return (
    <SocketProvider userId={userContext.userData._id}>
      <ContactsContextProvider userId={userContext.userData._id}>
        <ConversationsContextProvider
          userId={userContext.userData._id}
          username={userContext.userData.username}
        >
          <ModalContextProvider>
            <div className={css.app}>
              {routes}

              {previousLocation && (
                <Routes>
                  <Route path="/modal/*" element={<ModalManager />} />
                </Routes>
              )}
            </div>
          </ModalContextProvider>
        </ConversationsContextProvider>
      </ContactsContextProvider>
    </SocketProvider>
  );
};

export default App;
