import React from "react";
import { useTranslations } from "../contexts/translations";

export const useRequest = () => {
  const [isLoading, setIsLoading] = React.useState(false);
  const [error, setError] = React.useState<string | null>();
  const { translateServerMessages } = useTranslations();

  const activeHttpRequests = React.useRef<AbortController[]>([]);

  const sendRequest = React.useCallback(
    async (
      url: string,
      method = "GET",
      body: BodyInit | null = null,
      headers = {}
    ) => {
      setIsLoading(true);
      const httpAbortCtrl = new AbortController();
      activeHttpRequests.current.push(httpAbortCtrl);

      try {
        const response = await fetch(url, {
          method,
          body,
          headers,
          signal: httpAbortCtrl.signal,
        });

        const responseData = await response.json();

        activeHttpRequests.current = activeHttpRequests.current.filter(
          (reqCtrl) => reqCtrl !== httpAbortCtrl
        );

        if (!response.ok) {
          throw new Error(responseData.message);
        }

        setIsLoading(false);
        return responseData;
      } catch (err) {
        setError(translateServerMessages((err as Error).message));
        setIsLoading(false);
        throw err;
      }
    },
    [translateServerMessages]
  );

  const clearError = () => {
    setError(null);
  };

  React.useEffect(() => {
    return () => {
      activeHttpRequests.current.forEach((abortCtrl) => abortCtrl.abort());
    };
  }, []);

  return { isLoading, error, sendRequest, clearError };
};
