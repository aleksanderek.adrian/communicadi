import React from "react";

export interface Translations {
  uknownErr: string;
  logout: string;
  contacts: string;
  conversations: string;
  newMessageFrom: string;
  edit: string;
  delete: string;
  searchContacts: string;
  noContacts: string;
  addNewContact: string;
  searchUsersByUsername: string;
  addToContacts: string;
  sendDirectMessage: string;
  searchUsers: string;
  search: string;
  alreadyAdded: string;
  searchReset: string;
  noUsersFound: string;
  usersFound: string;
  deleteConversation: string;
  you: string;
  users: string;
  newChat: string;
  canAlsoSearchUsersOffContactsList: string;
  noConversations: string;
  addConversation: string;
  searchRecipients: string;
  chats: string;
  wtriteMessage: string;
  send: string;
  noConversationSelected: string;
  sendFirstMessageTo: string;
  userNotOnContactsList: string;
  closeConversation: string;
  today: string;
  signingIn: string;
  username: string;
  password: string;
  signUp: string;
  noAccountYet: string;
  singIn: string;
  signingUp: string;
  confirmPassword: string;
  hasAccount: string;
  add: string;
  cancel: string;
  ok: string;
  yes: string;
  no: string;
  save: string;
  cantUpdateData: string;
  editContact: (username: string) => string;
  deleteConversationAsk: string;
  processCannotBeUndone: string;
  deleteContactAsk: (name: string | undefined, username: string) => string;
  addContactAsk: (username: string) => string;
  optionalContactName: string;
  checkCredentials: string;
  userAlreadyExist: string;
  passwordDontMatch: string;
  back: string;
}

const pl: Translations = {
  uknownErr: "Coś poszło nie tak. Spróbuj ponownie później.",
  logout: "Wyloguj",
  contacts: "Kontakty",
  conversations: "Konwersacje",
  newMessageFrom: "Nowa wiadomość od",
  edit: "Edytuj",
  delete: "Usuń",
  searchContacts: "Szukaj w kontaktach",
  noContacts: "Brak kontaków",
  addNewContact: "Dodaj nowy kontakt",
  searchUsersByUsername:
    "Możesz wyszukać osoby po unikalnej nazwie użytkownika",
  addToContacts: "Dodaj do kontaktów",
  sendDirectMessage: "Wyślij wiadomość",
  searchUsers: "Szukaj użytkowników",
  search: "Szukaj",
  alreadyAdded: "dodano",
  searchReset: "Resetuj wyszukanie",
  noUsersFound: "Nie znaleziono użytkowników",
  usersFound: "Znaleziono użytkowników",
  deleteConversation: "Usuń konwersację",
  you: "Ty",
  users: "Użytkownicy",
  newChat: "Nowy chat",
  canAlsoSearchUsersOffContactsList:
    "Możesz rownież wyszukać użytkowników spoza swojej listy kontaktów",
  noConversations: "Brak konwersacji",
  addConversation: "Dodaj konwersację",
  searchRecipients: "Szukaj odbiorców",
  chats: "Czaty",
  wtriteMessage: "Napisz wiadomość",
  send: "Wyślij",
  noConversationSelected: "Nie wybrano konwersacji",
  sendFirstMessageTo: "Wyślij pierwszą wiadomość do",
  userNotOnContactsList: "Użytkownik nie jest na liście kontaktów",
  closeConversation: "Zamknij konwersację",
  today: "Dziś",
  signingIn: "Logowanie",
  username: "Nazwa użytkownika",
  password: "Hasło",
  signUp: "Zarejestruj",
  noAccountYet: "Nie masz jeszcze konta?",
  singIn: "Zaloguj",
  signingUp: "Rejestracja",
  confirmPassword: "Potwierdź hasło",
  hasAccount: "Posiadasz już konto?",
  add: "Dodaj",
  cancel: "Anuluj",
  ok: "Okej",
  yes: "Tak",
  no: "Nie",
  save: "Zapisz",
  cantUpdateData: "Nie udało się zaktualizować danych",
  editContact: (username) => `Edytuj kontakt ${username}`,
  deleteConversationAsk: "Czy na pewno chcesz usunąć tą konwersację?",
  processCannotBeUndone: "Tego procesu nie mozna cofnąć",
  deleteContactAsk: (name: string | undefined, username: string) =>
    `Czy na pewno chcesz usunąć ${
      name ? `${name} | ${username}` : username
    } ze swojej listy kontaktów?`,
  addContactAsk: (username: string) =>
    `Dodaj użytkownika ${username} do swoich kontaktów`,
  optionalContactName: "Opcjonalna nazwa kontaktu",
  checkCredentials: "Login lub hasło jest nieprawidłowe",
  userAlreadyExist: "Taki użytkownik już istnieje",
  passwordDontMatch: "Hasła się nie zgadzają",
  back: "Cofnij"
};

enum Langs {
  Polish,
}

export type TranslationsContextType = {
  translations: Translations;
  changeLanguage: (lang: Langs) => void;
  translateServerMessages: (message: string) => string;
};

const TranslationsContext = React.createContext<TranslationsContextType>(
  {} as TranslationsContextType
);

export const useTranslations = () => React.useContext(TranslationsContext);

export const TranslationsProvider = (props: React.PropsWithChildren) => {
  const [currentTranslations, setCurrentTranslations] =
    React.useState<Translations>(pl);

  const changeLanguage = React.useCallback((lang: Langs) => {
    switch (lang) {
      case Langs.Polish:
        setCurrentTranslations(pl);
        break;
      default:
        setCurrentTranslations(pl);
        break;
    }
  }, []);

  const translateServerMessages = React.useCallback(
    (message: string) => {
      switch (message) {
        case "Check credentials.":
          return currentTranslations.checkCredentials;
        case "User already exist.":
          return currentTranslations.userAlreadyExist;
        default:
          return currentTranslations.uknownErr;
      }
    },
    [currentTranslations]
  );

  return (
    <TranslationsContext.Provider
      value={{
        translations: currentTranslations,
        changeLanguage,
        translateServerMessages,
      }}
    >
      {props.children}
    </TranslationsContext.Provider>
  );
};
