import React from "react";
import { useNavigate } from "react-router-dom";

export interface User {
  _id: string;
  username: string;
}

type UserContextType = {
  isLoggedIn: boolean;
  token: string | null;
  userData: User;
  key: string;
  login: (userData: User, token: string, key: string) => void;
  logout: () => void;
};

const UserContext = React.createContext<UserContextType>({} as UserContextType);

export const useUserContext = () => React.useContext(UserContext);

interface UserContextProviderProps {}

export const UserContextProvider = (
  props: React.PropsWithChildren<UserContextProviderProps>
) => {
  const navigate = useNavigate();

  const [token, setToken] = React.useState<string | null>(null);
  const [key, setKey] = React.useState<string>("");

  const [userData, setUserData] = React.useState<User>({
    username: "",
    _id: "",
  });

  const login = React.useCallback(
    (userData: User, token: string, key: string) => {
      localStorage.setItem(
        "userData",
        JSON.stringify({ userData, token, key })
      );
      setKey(key);
      setUserData(userData);
      setToken(token);
      navigate("/dashboard/conversations");
    },
    // eslint-disable-next-line
    []
  );

  const logout = React.useCallback(() => {
    setToken(null);
    setKey("");
    localStorage.clear();
    setUserData({ username: "", _id: "" });
    navigate("/");
    // eslint-disable-next-line
  }, []);

  React.useEffect(() => {
    const userData = localStorage.getItem("userData");
    if (userData) {
      const userDataParsed = JSON.parse(userData);
      login(userDataParsed.userData, userDataParsed.token, userDataParsed.key);
    }
  }, [login]);

  return (
    <UserContext.Provider
      value={{
        isLoggedIn: !!token,
        token,
        key,
        userData,
        login,
        logout,
      }}
    >
      {props.children}
    </UserContext.Provider>
  );
};
