import React from "react";
import { io, Socket } from "socket.io-client";
import { useUserContext } from "./user";

const URL = process.env.REACT_APP_API_URL;

export type SocketContextType = {
  socket: Socket | null;
  connected: boolean;
};

const SocketContext = React.createContext<SocketContextType>(
  {} as SocketContextType
);

export const useSocket = () => React.useContext(SocketContext);

interface SocketProviderProps {
  userId: string;
}

export const SocketProvider = (
  props: React.PropsWithChildren<SocketProviderProps>
) => {
  const [socket, setSocket] = React.useState<Socket | null>(null);
  const [connected, setConnected] = React.useState(false);

  const userContext = useUserContext();

  React.useEffect(() => {
    if (userContext.isLoggedIn) {
      const newSocket = io(URL!, {
        query: { userId: userContext.userData._id },
      });
      setSocket(newSocket);
      return () => {
        newSocket.close();
      };
    }
  }, [userContext.userData._id, userContext.isLoggedIn]);

  React.useEffect(() => {
    if (socket) {
      socket.on("disconnect", (reason) => {
        if (reason === "transport error" || reason === "ping timeout") {
        }
        setConnected(false);
      });

      socket.on("connect", () => {
        setConnected(true);
      });
    }
  }, [socket]);

  return (
    <SocketContext.Provider
      value={{
        socket,
        connected,
      }}
    >
      {props.children}
    </SocketContext.Provider>
  );
};
