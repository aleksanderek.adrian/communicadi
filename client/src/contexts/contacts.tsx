import React from "react";
import { User, useUserContext } from "./user";
import { useLocalStorage } from "../hooks/useLocalStorage";
import { useRequest } from "../hooks/useRequest";
import { Conversation } from "./conversations";
import { useSocket } from "./socket";

const URL = process.env.REACT_APP_API_URL;

export interface Contact extends User {
  name?: string;
}

export type ContactsContextType = {
  addContact: (contactToAdd: Contact) => Promise<void>;
  deleteContact: (contactId: string) => Promise<void>;
  getContacts: () => Contact[];
  getContact: (userId: string) => Contact | undefined;
  getRecipient: (conversation: Conversation) => Contact;
  fetchContacts: () => Promise<void>;
  editContact: (contactId: string, newName: string) => Promise<void>;
};

const ContactsContext = React.createContext<ContactsContextType>(
  {} as ContactsContextType
);

export const useContactsContext = () => React.useContext(ContactsContext);

interface ContactsContextProviderProps {
  userId: string;
}

export const ContactsContextProvider = (
  props: React.PropsWithChildren<ContactsContextProviderProps>
) => {
  const [contacts, setContacts] = useLocalStorage("contacts", []);
  const userContext = useUserContext();
  const { sendRequest } = useRequest();
  const { connected } = useSocket();

  const userId = props.userId;

  React.useEffect(() => {
    if(!userContext.isLoggedIn) {
      setContacts([])
    }
  }, [userContext.isLoggedIn, setContacts])

  const addContact = React.useCallback(
    async (contactToAdd: Contact) => {
      try {
        const responseData: Contact = await sendRequest(
          `${URL}/api/user/contacts/add`,
          "PUT",
          JSON.stringify({
            userId,
            contactId: contactToAdd._id,
            username: contactToAdd.username,
            name: contactToAdd.name,
          }),
          {
            "Content-Type": "application/json",
            Authorization: "Bearer " + userContext.token,
          }
        );

        if (responseData) {
          setContacts((prev: Contact[]) => [...prev, responseData]);
        }
      } catch (err) {
        throw err;
      }
    },
    [userId, userContext.token, setContacts, sendRequest]
  );

  const getContacts = React.useCallback(() => {
    return contacts as Contact[];
  }, [contacts]);

  const getContact = React.useCallback(
    (userId: string) => {
      const contact: Contact = contacts.find(
        (contact: User) => contact._id === userId
      );
      return contact ? contact : undefined;
    },
    [contacts]
  );

  const editContact = React.useCallback(
    async (contactId: string, newName: string) => {
      try {
        const responseData = await sendRequest(
          `${URL}/api/user/contacts/edit`,
          "PATCH",
          JSON.stringify({
            userId,
            contactId: contactId,
            name: newName,
          }),
          {
            "Content-Type": "application/json",
            Authorization: "Bearer " + userContext.token,
          }
        );

        if (responseData) {
          setContacts((prev: Contact[]) => {
            const updated = prev.map((c) => {
              if (c._id === contactId) {
                return {
                  ...c,
                  name: newName,
                };
              } else {
                return c;
              }
            });
            return updated;
          });
        }
      } catch (err) {
        throw err;
      }
    },
    [userContext.token, userId, setContacts, sendRequest]
  );

  const deleteContact = React.useCallback(
    async (contactId: string) => {
      try {
        const reasponseDate = await sendRequest(
          `${URL}/api/user/contacts/delete/${userId}/${contactId}`,
          "DELETE",
          undefined,
          {
            Authorization: "Bearer " + userContext.token,
          }
        );
        if (reasponseDate) {
          setContacts((prev: Contact[]) =>
            prev.filter((contact: User) => contact._id !== contactId)
          );
        }
      } catch (err) {
        throw err;
      }
    },
    [userId, userContext.token, setContacts, sendRequest]
  );

  // Only direct conversations are handled for now
  const getRecipient = React.useCallback(
    (conversation: Conversation) => {
      const recipients = conversation.participants.filter(
        (participant) => participant._id !== userId
      );
      const contact = getContact(recipients[0]._id);
      const result: Contact = contact ?? recipients[0];
      return result;
    },
    [userId, getContact]
  );

  const fetchContacts = React.useCallback(async () => {
    if (connected) {
      try {
        const responseData: { contacts: Contact[] } = await sendRequest(
          `${URL}/api/user/contacts/${userId}`,
          "GET",
          undefined,
          {
            Authorization: "Bearer " + userContext.token,
          }
        );
        if (responseData) {
          setContacts(responseData.contacts);
        }
      } catch (err) {
        throw err;
      }
    }
  }, [userContext.token, connected, userId, setContacts, sendRequest]);

  return (
    <ContactsContext.Provider
      value={{
        addContact,
        deleteContact,
        getContacts,
        getContact,
        getRecipient,
        fetchContacts,
        editContact,
      }}
    >
      {props.children}
    </ContactsContext.Provider>
  );
};
