import React from "react";
import { User } from "./user";
import { useConversationContext } from "./conversations";
import { Contact, useContactsContext } from "./contacts";
import { Modal } from "../components/modal/Modal";
import {
  Route,
  Routes,
  useLocation,
  useNavigate,
  useParams,
} from "react-router-dom";
import css from "../components/modal/Modal.module.scss";
import { useTranslations } from "./translations";

interface ModalProps {
  exitModal: () => void;
  openErrorModal?: () => void;
}

const AddContactModal = ({ exitModal, openErrorModal }: ModalProps) => {
  const contactsContext = useContactsContext();
  const { translations } = useTranslations();
  const [contactName, setContactName] = React.useState("");

  const { username, userId } = useParams();
  const [loading, setLoading] = React.useState(false);

  const addContact = async () => {
    if (userId && username) {
      try {
        setLoading(true);
        await contactsContext.addContact({
          _id: userId,
          username,
          name: contactName,
        });
      } catch {
        return openErrorModal && openErrorModal();
      }
      exitModal();
    }
  };

  const onKeyHandler = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === "Enter") {
      e.preventDefault();
      addContact();
    }
  };

  return (
    <Modal isOpen={true} loading={loading}>
      <div>
        <p>{username && translations.addContactAsk(username)}</p>
        <input
          onKeyDown={onKeyHandler}
          type="text"
          placeholder={translations.optionalContactName}
          onChange={(e) => setContactName(e.currentTarget.value)}
        />
        <div className={css.buttons}>
          <button onClick={addContact}>{translations.add}</button>
          <button onClick={exitModal}>{translations.cancel}</button>
        </div>
      </div>
    </Modal>
  );
};

const DeleteContact = ({ exitModal, openErrorModal }: ModalProps) => {
  const { contactId, name, username } = useParams();
  const [loading, setLoading] = React.useState(false);
  const { translations } = useTranslations();

  const contactsContext = useContactsContext();

  const onContactDelete = async () => {
    if (contactId) {
      try {
        setLoading(true);
        await contactsContext.deleteContact(contactId);
      } catch {
        return openErrorModal && openErrorModal();
      }
      exitModal();
    }
  };

  return (
    <Modal isOpen={true} loading={loading}>
      <div>
        <p>{username && translations.deleteContactAsk(name, username)}</p>
        <div className={css.buttons}>
          <button onClick={onContactDelete}>{translations.yes}</button>
          <button onClick={exitModal}>{translations.no}</button>
        </div>
      </div>
    </Modal>
  );
};

const DeleteConversation = ({ exitModal, openErrorModal }: ModalProps) => {
  const { conversationId } = useParams();
  const [loading, setLoading] = React.useState(false);
  const { translations } = useTranslations();

  const conversationContext = useConversationContext();

  const onConversationDelete = async () => {
    if (conversationId) {
      try {
        setLoading(true);
        await conversationContext.deleteConversation(conversationId);
      } catch (err) {
        return openErrorModal && openErrorModal();
      }

      exitModal();
    }
  };

  return (
    <Modal isOpen={true} loading={loading}>
      <div>
        <p>{translations.deleteConversationAsk}</p>
        <p>{translations.processCannotBeUndone}</p>
        <div className={css.buttons}>
          <button onClick={onConversationDelete}>{translations.yes}</button>
          <button onClick={exitModal}>{translations.no}</button>
        </div>
      </div>
    </Modal>
  );
};

const EditContact = ({ exitModal, openErrorModal }: ModalProps) => {
  const { contactId, name, username } = useParams();
  const [loading, setLoading] = React.useState(false);

  const contactsContext = useContactsContext();

  const [contactName, setContactName] = React.useState(name ?? "");
  const { translations } = useTranslations();

  const onContactEdit = async () => {
    if (contactId) {
      try {
        setLoading(true);
        await contactsContext.editContact(contactId, contactName);
      } catch {
        return openErrorModal && openErrorModal();
      }
      exitModal();
    }
  };
  const placeholder = name ? "" : translations.optionalContactName;

  const onKeyHandler = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === "Enter") {
      e.preventDefault();
      onContactEdit();
    }
  };

  return (
    <Modal isOpen={true} loading={loading}>
      <div>
        <p>{username && translations.editContact(username)}</p>
        <input
          onKeyDown={onKeyHandler}
          placeholder={placeholder}
          type="text"
          value={contactName}
          onChange={(e) => setContactName(e.currentTarget.value)}
        />

        <div className={css.buttons}>
          <button onClick={onContactEdit}>{translations.save}</button>
          <button onClick={exitModal}>{translations.cancel}</button>
        </div>
      </div>
    </Modal>
  );
};

const DataFetchError = ({ exitModal }: ModalProps) => {
  const { translations } = useTranslations();

  return (
    <Modal isOpen={true}>
      <div>
        <p>{translations.cantUpdateData}</p>

        <div className={css.buttons}>
          <button onClick={exitModal}>{translations.ok}</button>
        </div>
      </div>
    </Modal>
  );
};

const UnknownError = ({ exitModal }: ModalProps) => {
  const { translations } = useTranslations();

  return (
    <Modal isOpen={true}>
      <div>
        <p>{translations.uknownErr}</p>

        <div className={css.buttons}>
          <button onClick={exitModal}>{translations.ok}</button>
        </div>
      </div>
    </Modal>
  );
};

export const ModalManager = () => {
  const navigate = useNavigate();
  const location = useLocation();

  const exitModal = React.useCallback(
    () => navigate(location.state.previousLocation.pathname),
    [location.state.previousLocation.pathname, navigate]
  );

  const openErrorModal = React.useCallback(
    () =>
      navigate(`/modal/unknownError`, {
        state: { previousLocation: location.state.previousLocation },
      }),
    [location.state, navigate]
  );

  return (
    <Routes>
      <Route
        path="deleteConversation/:conversationId"
        element={
          <DeleteConversation
            exitModal={exitModal}
            openErrorModal={openErrorModal}
          />
        }
      />
      <Route
        path="addContact/:username/:userId"
        element={
          <AddContactModal
            exitModal={exitModal}
            openErrorModal={openErrorModal}
          />
        }
      />
      <Route
        path="deleteContact/:contactId/:username/:name?"
        element={
          <DeleteContact
            exitModal={exitModal}
            openErrorModal={openErrorModal}
          />
        }
      />
      <Route
        path="editContact/:contactId/:username/:name?"
        element={
          <EditContact exitModal={exitModal} openErrorModal={openErrorModal} />
        }
      />
      <Route
        path="dataFetchError"
        element={<DataFetchError exitModal={exitModal} />}
      />
      <Route
        path="unknownError"
        element={<UnknownError exitModal={exitModal} />}
      />
    </Routes>
  );
};

export type ModalContextType = {
  openAddContactModal: (contactToAdd: User) => void;
  openDeleteContactModal: (contactToDelete: Contact) => void;
  openEditContactModal: (contactToEdit: Contact) => void;
  openDeleteConversationModal: (conversationId: string) => void;
  openDataFetchErrorModal: () => void;
  openUknownErrorModal: () => void;
};

const ModalContext = React.createContext<ModalContextType>(
  {} as ModalContextType
);

export const useModalContext = () => React.useContext(ModalContext);

interface ContactsContextProviderProps {
  userId?: string;
}

export const ModalContextProvider = (
  props: React.PropsWithChildren<ContactsContextProviderProps>
) => {
  const navigate = useNavigate();
  const location = useLocation();

  const openUknownErrorModal = React.useCallback(() => {
    navigate(`/modal/unknownError`, { state: { previousLocation: location } });
  }, [location, navigate]);

  const openDataFetchErrorModal = React.useCallback(() => {
    navigate(`/modal/dataFetchError`, {
      state: { previousLocation: location },
    });
  }, [location, navigate]);

  const openAddContactModal = React.useCallback(
    (contactToAdd: User) => {
      const username = contactToAdd.username;
      const userId = contactToAdd._id;
      navigate(`/modal/addContact/${username}/${userId}`, {
        state: { previousLocation: location },
      });
    },
    [location, navigate]
  );

  const openDeleteConversationModal = React.useCallback(
    (conversationId: string) => {
      navigate(`/modal/deleteConversation/${conversationId}`, {
        state: { previousLocation: location },
      });
    },
    [location, navigate]
  );

  const openEditContactModal = React.useCallback(
    (contactToEdit: Contact) => {
      const username = contactToEdit.username;
      const name = contactToEdit.name;
      const userId = contactToEdit._id;
      if (name) {
        navigate(`/modal/editContact/${userId}/${username}/${name}`, {
          state: { previousLocation: location },
        });
      } else {
        navigate(`/modal/editContact/${userId}/${username}`, {
          state: { previousLocation: location },
        });
      }
    },
    [location, navigate]
  );

  const openDeleteContactModal = React.useCallback(
    (contactToDelete: Contact) => {
      const username = contactToDelete.username;
      const name = contactToDelete.name;
      const userId = contactToDelete._id;
      if (name) {
        navigate(`/modal/deleteContact/${userId}/${username}/${name}`, {
          state: { previousLocation: location },
        });
      } else {
        navigate(`/modal/deleteContact/${userId}/${username}`, {
          state: { previousLocation: location },
        });
      }
    },
    [location, navigate]
  );

  return (
    <ModalContext.Provider
      value={{
        openAddContactModal,
        openDeleteContactModal,
        openEditContactModal,
        openDeleteConversationModal,
        openDataFetchErrorModal,
        openUknownErrorModal,
      }}
    >
      {props.children}
    </ModalContext.Provider>
  );
};
