import React from "react";
import { User, useUserContext } from "./user";
import { useLocalStorage } from "../hooks/useLocalStorage";
import { useLocation, useNavigate } from "react-router-dom";
import { useRequest } from "../hooks/useRequest";
import { v4 as uuidv4 } from "uuid";
import { useSocket } from "./socket";
import CryptoJs from "crypto-js";
import appLogo from "../assets/favicon.png";
import { useContactsContext } from "./contacts";

const URL = process.env.REACT_APP_API_URL;

export type MessageStatus = "read" | "sent" | "received" | "created";

export interface Message {
  id: string;
  sender: User;
  recipients: User[];
  content: string;
  status: MessageStatus;
  createdAt: Date;
  sentAt?: Date;
}

interface MessageToSent extends Message {
  conversationId: string;
}

export interface Conversation {
  id: string;
  participants: User[];
  messages: Message[];
}

export type ConversationContextType = {
  getSelectedConversation: () => Conversation | null;
  getConversations: () => Conversation[];
  selectConversation: (conversation: Conversation) => void;
  goToPrivateConversation: (recipient: User) => void;
  clearSelectedConversation: () => void;
  sendMessage: (
    content: string,
    recipients: User[],
    goToConversation?: boolean
  ) => Promise<void>;
  recieveMessage: ({ message }: { message: Message }) => Promise<void>;
  fetchConversations: () => Promise<void>;
  fetchInbox: () => Promise<void>;
  decpryptMessageContent: (content: string) => string;
  getLastMessageDate: (conversation: Conversation) => Date;
  notification: Message | null;
  clearNotification: () => void;
  readMessages: () => void;
  deleteConversation: (conversationId: string) => Promise<void>;
};

const ConversationsContext = React.createContext<ConversationContextType>(
  {} as ConversationContextType
);

export const useConversationContext = () =>
  React.useContext(ConversationsContext);

interface ConversationsContextProviderProps {
  userId: string;
  username: string;
}

export const ConversationsContextProvider = (
  props: React.PropsWithChildren<ConversationsContextProviderProps>
) => {
  const [conversations, setConversations] = useLocalStorage(
    "conversations",
    []
  );
  const [selectedConversationIndex, setSelectedConversationIndex] =
    React.useState<number | null>(null);
  const { sendRequest } = useRequest();
  const userContext = useUserContext();
  const contactsContext = useContactsContext();
  const AES_key = userContext.key;
  const [notification, setNotification] = React.useState<Message | null>(null);
  const location = useLocation();

  const [outbox, setOutbox] = React.useState<MessageToSent[]>([]);
  const [messagesToStore, setMessagesToStore] = React.useState<MessageToSent[]>(
    []
  );
  const [messagesToAlter, setMessagesToAlter] = React.useState<MessageToSent[]>(
    []
  );

  const clearNotification = React.useCallback(
    () => setNotification(null),
    [setNotification]
  );

  const navigate = useNavigate();

  const { socket, connected } = useSocket();

  React.useEffect(() => {
    if (!userContext.isLoggedIn) {
      setConversations([]);
      clearNotification();
      setMessagesToStore([]);
      setOutbox([]);
      setMessagesToAlter([]);
      setSelectedConversationIndex(null);
    }
  }, [
    userContext.isLoggedIn,
    clearNotification,
    setConversations,
    setMessagesToAlter,
    setMessagesToStore,
    setOutbox,
    setSelectedConversationIndex,
  ]);

  React.useEffect(() => {
    if (outbox.length > 0 && socket) {
      outbox.forEach((messageToSend) => {
        socket.emit("send-message", {
          message: messageToSend,
          conversationId: messageToSend.conversationId,
        });
      });

      setOutbox([]);
    }
  }, [outbox, socket]);

  const getSelectedConversation = React.useCallback(() => {
    if (selectedConversationIndex !== null && selectedConversationIndex >= 0) {
      return conversations[selectedConversationIndex] as Conversation;
    }
    return null;
  }, [selectedConversationIndex, conversations]);

  const getLastMessageDate = React.useCallback((conversation: Conversation) => {
    const date =
      conversation.messages[conversation.messages.length - 1].sentAt ??
      conversation.messages[conversation.messages.length - 1].createdAt;
    return new Date(date);
  }, []);

  const getInbox = React.useCallback(async () => {
    try {
      if (props.userId) {
        const responseData: { messages: Message[] } = await sendRequest(
          `${URL}/api/user/conversations/inbox/${props.userId}`,
          "GET",
          undefined,
          {
            Authorization: "Bearer " + userContext.token,
          }
        );

        await sendRequest(
          `${URL}/api/user/conversations/inbox/clear/${props.userId}`,
          "DELETE",
          undefined,
          {
            Authorization: "Bearer " + userContext.token,
          }
        );

        return responseData.messages;
      }
    } catch (err) {
      throw err;
    }
  }, [props.userId, userContext.token, sendRequest]);

  const areParticipantsEqual = React.useCallback(
    (arr1: User[], arr2: User[]): boolean => {
      if (arr1.length !== arr2.length) {
        return false;
      }

      const areUsersEqual = arr1.every((user1) =>
        arr2.some(
          (user2) =>
            user2._id === user1._id && user2.username === user1.username
        )
      );

      return areUsersEqual;
    },
    []
  );

  const sendMessage = React.useCallback(
    async (content: string, recipients: User[], goToConversation = false) => {
      const createdMessage: Message = {
        id: uuidv4(),
        sender: {
          _id: props.userId,
          username: props.username,
        },
        recipients,
        content: CryptoJs.AES.encrypt(content, AES_key).toString(),
        status: "created",
        createdAt: new Date(),
      };

      const participants = [
        createdMessage.sender,
        ...createdMessage.recipients,
      ];

      const foundConversationIndex = conversations.findIndex(
        (conversation: Conversation) =>
          areParticipantsEqual(conversation.participants, participants)
      );

      let conversation: Conversation | null = null;
      let conversationIndex: number | null = null;

      setConversations((prevConversations: Conversation[]) => {
        const updatedConversations = [...prevConversations];
        if (foundConversationIndex >= 0) {
          updatedConversations[foundConversationIndex].messages.push(
            createdMessage
          );
          conversation = updatedConversations[foundConversationIndex];
          conversationIndex = foundConversationIndex;
        } else {
          const createdConversation = {
            id: uuidv4(),
            participants,
            messages: [createdMessage],
          };
          updatedConversations.push(createdConversation);
          conversation = createdConversation;
          conversationIndex = updatedConversations.length - 1;
        }

        setOutbox((prev) => {
          return [
            ...prev,
            {
              ...createdMessage,
              conversationIndex: conversationIndex ?? -1,
              conversationId: conversation?.id ?? "",
            },
          ];
        });

        return updatedConversations;
      });

      if (goToConversation) {
        setSelectedConversationIndex(conversationIndex);
        navigate(`/dashboard/conversations`);
      }
    },
    [
      conversations,
      props.userId,
      props.username,
      AES_key,
      setConversations,
      navigate,
      areParticipantsEqual,
    ]
  );

  const decpryptMessageContent = React.useCallback(
    (content: string) =>
      CryptoJs.AES.decrypt(content, userContext.key).toString(
        CryptoJs.enc.Utf8
      ),
    [userContext.key]
  );

  const triggerNotification = React.useCallback(
    (message: Message) => {
      const selectedConversation = getSelectedConversation();

      if (
        !selectedConversation ||
        (selectedConversation &&
          !areParticipantsEqual(selectedConversation.participants, [
            message.sender,
            ...message.recipients,
          ])) ||
        location.pathname !== "/dashboard/conversations"
      ) {
        const contact = contactsContext.getContact(message.sender._id);
        const notificationBody = decpryptMessageContent(message.content);
        const notificationTitle = contact?.name
          ? `${contact.name} | ${contact.username}`
          : message.sender.username;

        if (Notification && Notification.permission !== "denied") {
          if (Notification.permission === "default") {
            Notification.requestPermission().then((permission) => {
              if (permission === "granted") {
                new Notification(notificationTitle, {
                  body: notificationBody,
                  icon: appLogo,
                });
              }
            });
          } else {
            new Notification(notificationTitle, {
              body: notificationBody,
              icon: appLogo,
            });
          }
        }
        setNotification(message);
      }
    },
    [
      setNotification,
      areParticipantsEqual,
      getSelectedConversation,
      location.pathname,
      decpryptMessageContent,
      contactsContext,
    ]
  );

  const deleteConversationInDb = React.useCallback(
    async (conversationId: string) => {
      if (conversationId) {
        try {
          await sendRequest(
            `${URL}/api/user/conversations/delete/${props.userId}/${conversationId}`,
            "DELETE",
            undefined,
            {
              Authorization: "Bearer " + userContext.token,
            }
          );
          return true;
        } catch (err) {
          throw err;
        }
      }
      return false;
    },
    [userContext.token, props.userId, sendRequest]
  );

  const deleteConversation = React.useCallback(
    async (conversationId: string) => {
      try {
        const deleted = await deleteConversationInDb(conversationId);
        if (deleted) {
          if (
            selectedConversationIndex &&
            conversations[selectedConversationIndex].id === conversationId
          ) {
            setSelectedConversationIndex(null);
          }

          setConversations((prev: Conversation[]) => {
            return prev.filter((conv) => conv.id !== conversationId);
          });
        }
      } catch (err) {
        throw err;
      }
    },
    [
      conversations,
      deleteConversationInDb,
      selectedConversationIndex,
      setConversations,
    ]
  );

  const readMessages = React.useCallback(() => {
    if (selectedConversationIndex !== null) {
      const selected: Conversation = conversations[selectedConversationIndex];

      if (
        selected.messages[selected.messages.length - 1].status === "received"
      ) {
        if (notification) {
          if (
            areParticipantsEqual(selected.participants, [
              notification.sender,
              ...notification.recipients,
            ])
          ) {
            clearNotification();
          }
        }

        setConversations((prev: Conversation[]) => {
          const updated = prev.map((conversation, index) => {
            if (index === selectedConversationIndex) {
              const updatedMessages = conversation.messages.map((message) => {
                if (message.status === "received") {
                  setMessagesToAlter((prev) => {
                    return [
                      ...prev,
                      {
                        ...message,
                        status: "read",
                        conversationId: conversation.id,
                      },
                    ];
                  });
                  return {
                    ...message,
                    status: "read",
                  };
                } else {
                  return message;
                }
              });

              return {
                ...conversation,
                messages: updatedMessages,
              };
            }
            return conversation;
          });

          return updated;
        });
      }
    }
  }, [
    selectedConversationIndex,
    conversations,
    notification,
    setConversations,
    areParticipantsEqual,
    clearNotification,
  ]);

  const recieveMessage = React.useCallback(
    async ({ message }: { message: Message }) => {
      const recievedMessage: Message = {
        ...message,
        status: "received",
      };

      triggerNotification(recievedMessage);

      const participants = [
        recievedMessage.sender,
        ...recievedMessage.recipients,
      ];
      const foundConversationIndex = conversations.findIndex(
        (conversation: Conversation) =>
          areParticipantsEqual(conversation.participants, participants)
      );

      let conversation: Conversation;

      if (foundConversationIndex >= 0) {
        setConversations((prevConversations: Conversation[]) => {
          const updatedConversations = [...prevConversations];
          updatedConversations[foundConversationIndex].messages.push(
            recievedMessage
          );
          return updatedConversations;
        });

        conversation = conversations[foundConversationIndex];
      } else {
        const createdConversation: Conversation = {
          id: uuidv4(),
          participants,
          messages: [recievedMessage],
        };

        setConversations((prevConversations: Conversation[]) => [
          ...prevConversations,
          createdConversation,
        ]);

        conversation = createdConversation;
      }
      setMessagesToStore((prev) => {
        return [
          ...prev,
          {
            ...recievedMessage,
            conversationIndex: foundConversationIndex ?? -1,
            conversationId: conversation?.id ?? "",
          },
        ];
      });
    },
    [conversations, triggerNotification, setConversations, areParticipantsEqual]
  );

  const goToPrivateConversation = React.useCallback(
    (recipient: User) => {
      if (recipient) {
        const user: User = {
          username: props.username,
          _id: props.userId,
        };
        const participants = [user, recipient];

        const foundConversationIndex = conversations.findIndex(
          (conversation: Conversation) =>
            areParticipantsEqual(conversation.participants, participants)
        );

        if (foundConversationIndex >= 0) {
          setSelectedConversationIndex(foundConversationIndex);
          navigate(`/dashboard/conversations`);
        } else {
          setSelectedConversationIndex(null);

          navigate(
            `/dashboard/conversations/new/${recipient._id}/${recipient.username}`
          );
        }
      }
    },
    [
      props.username,
      props.userId,
      conversations,
      navigate,
      areParticipantsEqual,
    ]
  );

  const storeMessageInDb = React.useCallback(
    async (message: MessageToSent) => {
      if (message) {
        try {
          await sendRequest(
            `${URL}/api/user/conversations/storeMessage/${props.userId}`,
            "PUT",
            JSON.stringify({
              conversationId: message.conversationId,
              message,
            }),
            {
              "Content-Type": "application/json",
              Authorization: "Bearer " + userContext.token,
            }
          );
          return true;
        } catch (err) {
          return false;
        }
      }
      return false;
    },
    [props.userId, userContext.token, sendRequest]
  );

  const alterMessageInDb = React.useCallback(
    async (message: MessageToSent) => {
      if (message) {
        try {
          await sendRequest(
            `${URL}/api/user/conversations/alterMessage/${props.userId}`,
            "PUT",
            JSON.stringify({
              conversationId: message.conversationId,
              message,
            }),
            {
              "Content-Type": "application/json",
              Authorization: "Bearer " + userContext.token,
            }
          );
          return true;
        } catch (err) {
          return false;
        }
      }
      return false;
    },
    [props.userId, userContext.token, sendRequest]
  );

  React.useEffect(() => {
    const alterMessages = async () => {
      if (messagesToAlter.length > 0) {
        const messageToAlter = messagesToAlter[0];
        await alterMessageInDb(messageToAlter);
        setMessagesToAlter((prev) => {
          const updated = [...prev];
          updated.shift();
          return updated;
        });
      }
    };

    alterMessages();
  }, [messagesToAlter, alterMessageInDb]);

  React.useEffect(() => {
    const storeMessages = async () => {
      if (messagesToStore.length > 0) {
        const messageToStore = messagesToStore[0];
        await storeMessageInDb(messageToStore);
        setMessagesToStore((prev) => {
          const updated = [...prev];
          updated.shift();
          return updated;
        });
      }
    };

    storeMessages();
  }, [messagesToStore, storeMessageInDb]);

  const getConversations = React.useCallback(() => {
    return conversations;
  }, [conversations]);

  const selectConversation = React.useCallback(
    (conversation: Conversation) => {
      const foundConversationIndex = (
        conversations as Conversation[]
      ).findIndex((c) => c.id === conversation.id);

      if (foundConversationIndex >= 0) {
        setSelectedConversationIndex(foundConversationIndex);
      } else {
        setSelectedConversationIndex(null);
      }
    },
    [conversations]
  );

  const clearSelectedConversation = React.useCallback(() => {
    setSelectedConversationIndex(null);
  }, []);

  const fetchConversations = React.useCallback(async () => {
    if (connected) {
      try {
        const responseData: { conversations: Conversation[] } =
          await sendRequest(
            `${URL}/api/user/conversations/${props.userId}`,
            "GET",
            undefined,
            {
              Authorization: "Bearer " + userContext.token,
            }
          );
        if (responseData) {
          setConversations(responseData.conversations);
        }
      } catch (err) {
        throw err;
      }
    }
  }, [
    props.userId,
    connected,
    userContext.token,
    setConversations,
    sendRequest,
  ]);

  const fetchInbox = React.useCallback(async () => {
    if (connected) {
      try {
        const messagesFromInbox = await getInbox();

        if (messagesFromInbox) {
          setConversations((prevConversations: Conversation[]) => {
            const updatedConversations = [...prevConversations];

            messagesFromInbox.forEach((message) => {
              const participants = [message.sender, ...message.recipients];
              const foundConversationIndex = updatedConversations.findIndex(
                (conversation) =>
                  areParticipantsEqual(conversation.participants, participants)
              );
              let conversation: Conversation;
              const recievedMessage: Message = {
                ...message,
                status: "received",
              };

              if (foundConversationIndex >= 0) {
                updatedConversations[foundConversationIndex].messages.push(
                  recievedMessage
                );
                conversation = updatedConversations[foundConversationIndex];
              } else {
                const createdConversation: Conversation = {
                  id: uuidv4(),
                  participants,
                  messages: [recievedMessage],
                };
                updatedConversations.push(createdConversation);
                conversation =
                  updatedConversations[updatedConversations.length - 1];
              }

              setMessagesToStore((prev) => {
                return [
                  ...prev,
                  {
                    ...message,
                    conversationIndex: foundConversationIndex ?? -1,
                    conversationId: conversation?.id ?? "",
                  },
                ];
              });
            });

            return updatedConversations;
          });
        }
      } catch (err) {
        throw err;
      }
    }
  }, [connected, getInbox, setConversations, areParticipantsEqual]);

  const messageSent = React.useCallback(
    ({
      message,
      conversationId,
    }: {
      message: Message;
      conversationId: string;
    }) => {
      setConversations((prevConversations: Conversation[]) => {
        const updatedConversations = [...prevConversations];
        const foundConversationIndex = updatedConversations.findIndex(
          (c) => c.id === conversationId
        );
        if (foundConversationIndex >= 0) {
          const foundMessageIndex = updatedConversations[
            foundConversationIndex
          ].messages.findIndex((m) => m.id === message.id);

          if (foundMessageIndex >= 0) {
            updatedConversations[foundConversationIndex].messages[
              foundMessageIndex
            ] = message;
            setMessagesToStore((prev) => {
              return [
                ...prev,
                {
                  ...message,
                  foundConversationIndex,
                  conversationId:
                    updatedConversations[foundConversationIndex].id,
                },
              ];
            });
          } else {
            // Another user device
            updatedConversations[foundConversationIndex].messages.push(message);
          }
        }
        return updatedConversations;
      });
    },
    [setConversations]
  );

  React.useEffect(() => {
    if (socket) {
      socket.on("receive-message", recieveMessage);
      socket.on("message-sent", messageSent);

      return () => {
        socket.off("receive-message", recieveMessage);
        socket.off("message-sent", messageSent);
      };
    }
  }, [socket, recieveMessage, messageSent]);

  return (
    <ConversationsContext.Provider
      value={{
        getLastMessageDate,
        getConversations,
        selectConversation,
        getSelectedConversation,
        goToPrivateConversation,
        clearSelectedConversation,
        sendMessage,
        recieveMessage,
        fetchConversations,
        fetchInbox,
        decpryptMessageContent,
        notification,
        clearNotification,
        readMessages,
        deleteConversation,
      }}
    >
      {props.children}
    </ConversationsContext.Provider>
  );
};
