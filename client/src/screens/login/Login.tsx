import React from "react";
import css from "./Login.module.scss";
import { Link } from "react-router-dom";
import FormInput from "../../components/input/FormInput";
import { LoadingModal } from "../../components/loading/LoadingSpinner";
import { useUserContext } from "../../contexts/user";
import { useRequest } from "../../hooks/useRequest";
import { useTranslations } from "../../contexts/translations";

const URL = process.env.REACT_APP_API_URL;

export const Logo = () => {
  return <h2 className={css.logo}>{`>Communicadi`}</h2>;
};

const Login = () => {
  const userContext = useUserContext();
  const { sendRequest, isLoading, error } = useRequest();
  const { translations } = useTranslations();

  const login = async (password: string, username: string) => {
    try {
      const responseData = await sendRequest(
        `${URL}/api/user/login`,
        "POST",
        JSON.stringify({
          username,
          password,
        }),
        {
          "Content-Type": "application/json",
        }
      );
      if (responseData) {
        userContext.login(
          { _id: responseData._id, username: responseData.username },
          responseData.token,
          responseData.key
        );
      }
    } catch {}
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const form = e.currentTarget;
    form.classList.add(css.submitet);

    const isValid = form.checkValidity();
    if (isValid) {
      const username = (form.elements.namedItem("username") as HTMLInputElement)
        .value;
      const password = (form.elements.namedItem("password") as HTMLInputElement)
        .value;
      await login(password, username);
    }
  };

  return (
    <div className={css.main}>
      {isLoading && <LoadingModal />}
      <form className={css.box} noValidate onSubmit={handleSubmit}>
        <Logo />
        <h3>{translations.signingIn}</h3>
        <div className={css.boxInputs}>
          <FormInput
            label={translations.username}
            required
            minLength={3}
            name={"username"}
            type="text"
          />
          <FormInput
            label={translations.password}
            required
            minLength={8}
            name={"password"}
            type="password"
          />
        </div>
        <p className={css.serverError}>{error}</p>
        <div className={css.boxBottom}>
          <div>
            <p>{translations.noAccountYet}</p>
            <Link to={`/register`}>{translations.signUp}</Link>
          </div>
          <button type="submit">{translations.singIn}</button>
        </div>
      </form>
    </div>
  );
};

export default Login;
