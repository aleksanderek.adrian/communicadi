import React from "react";
import css from "./ConversationsList.module.scss";
import SideList from "../../../components/sideList/SideList";
import {
  Conversation,
  useConversationContext,
} from "../../../contexts/conversations";
import { User, useUserContext } from "../../../contexts/user";
import { useNavigate } from "react-router-dom";
import { ReactComponent as AddIcon } from "../../../assets/add.svg";
import { ReactComponent as BackIcon } from "../../../assets/back.svg";
import { useModalContext } from "../../../contexts/modal";
import { Contact, useContactsContext } from "../../../contexts/contacts";
import { useRequest } from "../../../hooks/useRequest";
import { FoundUserI } from "../../contacts/contactsFinder/ContactsFinder";
import Dropdown from "../../../components/dropdown/Dropdown";
import { ReactComponent as Letter } from "../../../assets/letter.svg";
import { getTime } from "../../../helpers";
import { Tooltip } from "react-tooltip";
import { ReactComponent as SearchIcon } from "../../../assets/search.svg";
import { LoadingSpinner } from "../../../components/loading/LoadingSpinner";
import { ReactComponent as InfoIcon } from "../../../assets/info.svg";
import { Translations, useTranslations } from "../../../contexts/translations";

const URL = process.env.REACT_APP_API_URL;

interface ConversationItemProps {
  data: Conversation;
  userId: string;
  selectConversation: (conversation: Conversation) => void;
  selectedConversationId: string;
  decryptMessage: (content: string) => string;
  newMessage?: boolean;
  getContact: (userId: string) => Contact | undefined;
  deleteConversation: (conversationId: string) => void;
  translations: Translations;
}

const ConversationItem = (props: ConversationItemProps) => {
  const lastMessage = props.data.messages[props.data.messages.length - 1];
  const lastMessageDate = new Date(lastMessage.sentAt ?? lastMessage.createdAt);
  const isLastMessageSentByUser = lastMessage.sender._id === props.userId;
  const isSelected = props.data.id === props.selectedConversationId;
  const isNewMessage = lastMessage.status === "received";
  const recipient = props.data.participants.find(
    (participant) => participant._id !== props.userId
  );

  const onClickHandler = () => {
    props.selectConversation(props.data);
  };

  const onDeleteConv = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    e.stopPropagation();
    props.deleteConversation(props.data.id);
  };

  const depryctedLastMessage = props.decryptMessage(lastMessage.content);

  const contact = props.getContact(recipient?._id ?? "");

  return (
    <li
      className={`${css.conversation} ${isSelected ? css.selected : ""}`}
      onClick={onClickHandler}
    >
      <div>
        <div className={css.topBar}>
          <div className={css.info}>
            {isNewMessage && <Letter className={css.letter} />}

            <Dropdown>
              <button onClick={onDeleteConv}>
                {props.translations.deleteConversation}
              </button>
            </Dropdown>
          </div>
          {contact && (
            <div className={`${css.name} ${css.textOverflow}`}>
              {contact.name}
            </div>
          )}
        </div>

        <div
          className={`${css.user} ${css.textOverflow}`}
          style={{ opacity: contact && contact.name ? 0.5 : 1 }}
        >
          {recipient?.username}
        </div>
        <div className={`${css.message} ${css.textOverflow}`}>
          {isLastMessageSentByUser && (
            <span>{`${props.translations.you}:`}</span>
          )}
          <span>{depryctedLastMessage}</span>
        </div>
        <div
          className={`${css.date} ${css.textOverflow}`}
        >{`${lastMessageDate.toLocaleDateString()} ${
          lastMessage.sentAt && getTime(new Date(lastMessage.sentAt), false)
        }`}</div>
      </div>
    </li>
  );
};

interface AsideProps {
  userId: string;
}

export const Aside = (props: AsideProps) => {
  const [newConversationMode, setNewConversationMode] = React.useState(false);

  return (
    <div className={css.main}>
      {newConversationMode ? (
        <FindNewChat setNewChat={setNewConversationMode} />
      ) : (
        <ConversationsList
          userId={props.userId}
          setNewChat={setNewConversationMode}
        />
      )}
    </div>
  );
};

const ContactItemSmall = ({
  contact,
  goToChat,
}: {
  contact: Contact;
  goToChat: (recipient: User) => void;
}) => {
  return (
    <div className={css.userItemSmall} onClick={() => goToChat(contact)}>
      {contact?.name
        ? `${contact.name} | ${contact.username}`
        : contact.username}
    </div>
  );
};

interface FindNewChatProps {
  setNewChat: React.Dispatch<React.SetStateAction<boolean>>;
}

const FindNewChat = (props: FindNewChatProps) => {
  const userContext = useUserContext();
  const contactsContext = useContactsContext();
  const conversationsContext = useConversationContext();
  const contacts = contactsContext.getContacts();
  const { sendRequest, error, isLoading } = useRequest();
  const [users, setUsers] = React.useState<FoundUserI[]>([]);
  const [searchQuery, setSearchQuery] = React.useState("");
  const [submited, setSubmitet] = React.useState<boolean>(false);
  const userId = userContext.userData._id;
  const { translations } = useTranslations();

  const performSearch = React.useCallback(async () => {
    if (searchQuery.length > 0) {
      setSubmitet(true);
      try {
        const responseData: { users: FoundUserI[] } = await sendRequest(
          `${URL}/api/users/${searchQuery}/${userId}`,
          "GET",
          undefined,
          {
            Authorization: "Bearer " + userContext.token,
          }
        );
        setUsers(responseData.users);
      } catch {}
    }
  }, [searchQuery, userContext.token, userId, sendRequest]);

  const onClickHandler = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.preventDefault();
    performSearch();
  };

  const onEnterHandler = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === "Enter") {
      e.preventDefault();
      performSearch();
    }
  };

  const filterFoundUsers = () => {
    return users.filter((user) => !user.isAlreadyAdded);
  };

  const foundUsers = () => {
    return (
      <div className={css.contactsList}>
        <p className={css.listTitle}>{`${translations.users}:`}</p>
        {isLoading ? (
          <LoadingSpinner small={true} />
        ) : filterFoundUsers().length > 0 ? (
          <div
            className={`${css.scrollList} ${css.foundUsers} customScrollBar`}
          >
            {filterFoundUsers().map((user, index) => (
              <ContactItemSmall
                contact={user}
                goToChat={goToChat}
                key={`found-user-item-small-${index}`}
              />
            ))}
          </div>
        ) : error ? (
          <p>{error}</p>
        ) : (
          <p>{translations.noUsersFound}</p>
        )}
      </div>
    );
  };

  const goToChat = (recipient: User) => {
    props.setNewChat(false);
    conversationsContext.goToPrivateConversation(recipient);
  };

  const concatName = (contact: Contact) => {
    return contact.name + contact.username;
  };
  const contactsList = () => {
    const filteredContacts = contacts.filter(
      (c) => concatName(c).indexOf(searchQuery) !== -1
    );
    return (
      <div className={css.contactsList}>
        <p className={css.listTitle}>{`${translations.contacts}:`}</p>
        {filteredContacts.length > 0 ? (
          <div className={`${css.scrollList} customScrollBar`}>
            {filteredContacts.map((contact, index) => (
              <ContactItemSmall
                contact={contact}
                goToChat={goToChat}
                key={`contact-item-small-${index}`}
              />
            ))}
          </div>
        ) : (
          <p>{translations.noContacts}</p>
        )}
      </div>
    );
  };

  return (
    <div>
      <div className={css.header}>
        <div className={css.title}>
          <div>{translations.newChat}</div>
          <button
            data-tooltip-id="back-tooltip"
            data-tooltip-content={translations.back}
            onClick={() => props.setNewChat(false)}
          >
            <Tooltip className="tooltip" delayShow={500} id="back-tooltip" />

            <BackIcon className={css.backButton} />
          </button>
        </div>
        <form className={css.inputWrapper}>
          <input
            placeholder={translations.searchUsers}
            onKeyDown={onEnterHandler}
            type="text"
            onChange={(e) => setSearchQuery(e.currentTarget.value)}
          />
          <button
            data-tooltip-id="search-tooltip"
            data-tooltip-content={translations.search}
            className={css.searchButton}
            onClick={onClickHandler}
          >
            <Tooltip className="tooltip" delayShow={500} id="search-tooltip" />
            <SearchIcon className={css.searchIcon} />
          </button>
        </form>
      </div>
      <p className={css.newConvInfo}>
        <InfoIcon className={css.infoIcon} />
        {translations.canAlsoSearchUsersOffContactsList}
      </p>
      {submited && foundUsers()}
      {contactsList()}
    </div>
  );
};

interface ConversationsListProps {
  setNewChat: React.Dispatch<React.SetStateAction<boolean>>;
  userId: string;
}

const ConversationsList = (props: ConversationsListProps) => {
  const conversationsContext = useConversationContext();
  const modalContext = useModalContext();
  const contactsContext = useContactsContext();
  const { translations } = useTranslations();

  const navigate = useNavigate();

  const selectConversationAndNavigate = (conversation: Conversation) => {
    conversationsContext.selectConversation(conversation);
    navigate("/dashboard/conversations");
  };

  const conversations = conversationsContext.getConversations();
  const selectedConversation = conversationsContext.getSelectedConversation();

  const compareDates = (a: Conversation, b: Conversation): number => {
    const dateA = conversationsContext.getLastMessageDate(a);
    const dateB = conversationsContext.getLastMessageDate(b);

    return dateA.getTime() - dateB.getTime();
  };

  const sortedConversations = conversations
    .slice()
    .sort(compareDates)
    .reverse();

  const [searchText, setSearchText] = React.useState("");

  const filterNick = (p: User[]) => {
    const recipient = p.find((participant) => participant._id !== props.userId);
    return recipient?.username;
  };

  const filteredConversations = sortedConversations.filter(
    (c) => filterNick(c.participants)?.indexOf(searchText) !== -1
  );

  const conversationsList = (
    <SideList>
      {filteredConversations.map((conversation, index) => (
        <ConversationItem
          key={`conversation-item-${index}`}
          translations={translations}
          deleteConversation={modalContext.openDeleteConversationModal}
          getContact={contactsContext.getContact}
          decryptMessage={conversationsContext.decpryptMessageContent}
          selectedConversationId={selectedConversation?.id ?? ""}
          selectConversation={selectConversationAndNavigate}
          data={conversation}
          userId={props.userId}
        />
      ))}
    </SideList>
  );

  const noConversations = (
    <div className={css.noConversations}>{translations.noConversations}</div>
  );

  return (
    <div>
      <div className={css.header}>
        <div className={css.title}>
          <div>{translations.chats}</div>
          <button
            onClick={() => props.setNewChat(true)}
            data-tooltip-id="new-conv-tooltip"
            data-tooltip-content={translations.addConversation}
          >
            <Tooltip
              className="tooltip"
              delayShow={500}
              id="new-conv-tooltip"
            />

            <AddIcon className={css.newChatButton} />
          </button>
        </div>
        <input
          placeholder={translations.searchRecipients}
          onChange={(e) => setSearchText(e.currentTarget.value)}
          type="text"
          className={css.search}
        />
      </div>
      {filteredConversations.length === 0 ? noConversations : conversationsList}
    </div>
  );
};
