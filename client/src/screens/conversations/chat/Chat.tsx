import React from "react";
import css from "./Chat.module.scss";
import { ReactComponent as SendIcon } from "../../../assets/send.svg";
import {
  Conversation,
  Message,
  useConversationContext,
} from "../../../contexts/conversations";
import { useNavigate, useParams } from "react-router-dom";
import { useContactsContext } from "../../../contexts/contacts";
import { User, useUserContext } from "../../../contexts/user";
import { ReactComponent as DeleteIcon } from "../../../assets/delete.svg";
import { ReactComponent as AddIcon } from "../../../assets/add.svg";
import {
  AutoSizer,
  List,
  CellMeasurer,
  CellMeasurerCache,
} from "react-virtualized";
import { useModalContext } from "../../../contexts/modal";
import { getTime } from "../../../helpers";
import { Logo } from "../../login/Login";
import { Tooltip } from "react-tooltip";
import { useTranslations } from "../../../contexts/translations";

const cache = new CellMeasurerCache({
  fixedWidth: true,
  defaultHeight: 16,
});

interface InputBoxProps {
  newConversation: boolean;
  recipients: User[];
  scrollToBottom?: () => void;
}

const InputBox = (props: InputBoxProps) => {
  const inputRef = React.useRef<HTMLTextAreaElement>(null);
  React.useEffect(() => {
    const current = inputRef?.current;
    if (current) {
      current.focus();
    }
  }, [props.recipients, inputRef]);

  const { translations } = useTranslations();

  const [message, setMessage] = React.useState("");

  const conversationContext = useConversationContext();

  const handleSendMessage = async () => {
    if (message.length > 0) {
      setMessage("");
      await conversationContext.sendMessage(
        message,
        props.recipients,
        props.newConversation
      );
    }
  };

  const handleEnter = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
    if (e.key === "Enter" && e.shiftKey) {
      e.preventDefault();
    } else if (e.key === "Enter") {
      e.preventDefault();
      handleSendMessage();
    }
  };

  return (
    <div className={css.inputBoxWrapper}>
      <div className={css.inputBox}>
        <textarea
          placeholder={translations.wtriteMessage + "..."}
          ref={inputRef}
          onKeyDown={handleEnter}
          value={message}
          className="customScrollBar"
          onChange={(e) => setMessage(e.currentTarget.value)}
        />
        <div
          className={css.sendIconWrapper}
          data-tooltip-id="exit-tooltip"
          data-tooltip-content={translations.send}
        >
          <Tooltip id="send-tooltip" className="tooltip" delayShow={500} />
          <SendIcon onClick={() => handleSendMessage()} />
        </div>
      </div>
    </div>
  );
};

interface ChatBubbleProps {
  message: Message;
  userId: string;
  decpryptMessage: (content: string) => string;
}

const ChatBubble = (props: ChatBubbleProps) => {
  const isIncoming = props.message.sender._id !== props.userId;
  const depryptedContent = props.decpryptMessage(props.message.content);

  let sentAt;
  if (props.message.sentAt) {
    sentAt = new Date(props.message.sentAt);
  }

  const unsent = (
    <div className={css.unsent}>
      {`[`}
      <span>00:00:00</span>
      {`]`}
    </div>
  );
  const messageDate = <div>{`[${getTime(sentAt)}]`}</div>;

  return (
    <li className={css.chatBubble}>
      <div className={css.time}>
        {sentAt ? messageDate : unsent}
        <div>{isIncoming ? ">>" : "<<"}</div>
      </div>
      <div className={`${isIncoming ? css.incoming : css.outgoing}`}>
        {depryptedContent}
      </div>
    </li>
  );
};

export const ConversationBody = () => {
  const conversationContext = useConversationContext();
  const selectedConversation = conversationContext.getSelectedConversation();

  return (
    <>
      {selectedConversation ? (
        <Chat selectedConversation={selectedConversation} />
      ) : (
        <NoChatSelected />
      )}
    </>
  );
};

const NoChatSelected = () => {
  const { translations } = useTranslations();
  return (
    <div className={css.main}>
      <div className={css.noConvSelected}>
        <Logo />
        <h1>{translations.noConversationSelected}</h1>
      </div>
    </div>
  );
};

export const NewConversation = () => {
  const contactsContext = useContactsContext();
  const { translations } = useTranslations();
  const { uid: participantId, username } = useParams();
  const contact = contactsContext.getContact(participantId ?? "");

  return (
    <div className={css.newConversation}>
      <Header
        newConversation={true}
        recipientId={participantId ?? ""}
        recipientUsername={username ?? ""}
      />
      <div className={css.content}>
        <h2>{translations.sendFirstMessageTo}</h2>
        {contact?.name && <p>{contact.name}</p>}
        <p>{username}</p>
      </div>
      <InputBox
        newConversation={true}
        recipients={[{ _id: participantId ?? "", username: username ?? "" }]}
      />
    </div>
  );
};

interface HeaderProps {
  recipientId: string;
  recipientUsername: string;
  newConversation: boolean;
}

const Header = (props: HeaderProps) => {
  const contactsContext = useContactsContext();
  const conversationContext = useConversationContext();
  const modalContext = useModalContext();
  const { translations } = useTranslations();
  const navigate = useNavigate();

  const contact = contactsContext.getContact(props.recipientId);

  const unselectConversation = () => {
    conversationContext.clearSelectedConversation();
    navigate("/dashboard/conversations");
  };

  const onAddContactClick = async () => {
    modalContext.openAddContactModal({
      _id: props.recipientId,
      username: props.recipientUsername,
    });
  };

  const withContactName = (
    <div>{`${contact?.name} | ${props.recipientUsername}`}</div>
  );
  const onlyUsername = <div>{props.recipientUsername}</div>;

  return (
    <div className={css.chatHeader}>
      <div className={css.chatHeader_top}>
        <div className={css.chatHeader_top_recipient}>
          {contact && contact.name ? withContactName : onlyUsername}
          <button
            className={css.addContactButton}
            onClick={onAddContactClick}
            data-tooltip-id="add-contact-tooltip"
            data-tooltip-content={translations.addToContacts}
          >
            <AddIcon
              style={{ visibility: contact ? "hidden" : "visible" }}
              className={css.addContact}
            />
            <Tooltip
              id="add-contact-tooltip"
              className="tooltip"
              delayShow={500}
            />
          </button>
        </div>
        {!contact && (
          <div className={css.chatHeader_bottom}>
            {translations.userNotOnContactsList}
          </div>
        )}
      </div>
      <button
        data-tooltip-id="unselect-conversation-tooltip"
        data-tooltip-content={translations.closeConversation}
        className={css.closeConvButton}
        onClick={() => unselectConversation()}
      >
        <Tooltip
          id="unselect-conversation-tooltip"
          className="tooltip"
          delayShow={500}
        />

        <DeleteIcon className={css.closeConv} />
      </button>
    </div>
  );
};

interface ChatProps {
  selectedConversation: Conversation;
}

const Chat = (props: ChatProps) => {
  const conversationContext = useConversationContext();
  const contactsContext = useContactsContext();
  const userContext = useUserContext();

  const listRef = React.useRef<List>(null);
  // eslint-disable-next-line
  const messages: Message[] = [...props.selectedConversation.messages]
  const recipient = contactsContext.getRecipient(props.selectedConversation);

  React.useEffect(() => {
    const listener = () => {
      cache.clearAll();
    };

    window.addEventListener("resize", listener);
    return () => window.removeEventListener("resize", listener);
  }, []);

  React.useLayoutEffect(() => {
    const current = listRef?.current;

    if (current) {
      requestAnimationFrame(() => {
        current.scrollToRow(messages.length - 1);
      });
    }
  }, [messages]);

  const compareDate = (date1: Date, date2: Date) => {
    if (date1 && date2) {
      date1 = new Date(date1);
      date2 = new Date(date2);

      if (date1.getFullYear() > date2.getFullYear()) {
        return true;
      } else if (date1.getFullYear() === date2.getFullYear()) {
        if (date1.getMonth() === date2.getMonth()) {
          if (date1.getDate() > date2.getDate()) {
            return true;
          } else {
            return false;
          }
        } else if (date1.getMonth() > date2.getMonth()) {
          return true;
        }
      } else {
        return false;
      }
    }
  };

  const Row = ({
    index,
    style,
    parent,
    key,
  }: {
    index: number;
    style: React.CSSProperties;
    parent: any;
    key: string;
  }) => {
    const currentMessageDate =
      messages[index].sentAt ?? messages[index].createdAt;
    const prevMessageDate =
      index - 1 >= 0
        ? messages[index - 1].sentAt ?? messages[index - 1].createdAt
        : undefined;

    let renderIndicator = false;
    let date = new Date();

    if (currentMessageDate && index === 0) {
      renderIndicator = true;
      date = new Date(currentMessageDate);
    } else if (prevMessageDate && currentMessageDate) {
      if (compareDate(currentMessageDate, prevMessageDate)) {
        renderIndicator = true;
        date = new Date(currentMessageDate);
      }
    } else {
      renderIndicator = false;
    }

    return (
      <CellMeasurer
        style={{ marginBottom: 10 }}
        key={key}
        cache={cache}
        parent={parent}
        columnIndex={0}
        rowIndex={index}
      >
        {({ measure }) => (
          <div style={style} className={css.messageWrapper} onLoad={measure}>
            {renderIndicator && <DateIndicator date={date} />}
            <ChatBubble
              userId={userContext.userData._id}
              message={messages[index]}
              decpryptMessage={conversationContext.decpryptMessageContent}
            />
          </div>
        )}
      </CellMeasurer>
    );
  };

  const readMessages = React.useCallback(() => {
    conversationContext.readMessages();
  },[conversationContext]);

  React.useEffect(() => {
    readMessages();
  }, [recipient, readMessages]);

  return (
    <div className={css.main} onFocus={readMessages} onClick={readMessages}>
      <Header
        newConversation={false}
        recipientId={recipient._id ?? ""}
        recipientUsername={recipient?.username ?? ""}
      />

      <div className={css.listWrapper}>
        <AutoSizer>
          {({ height, width }) => (
            <List
              ref={listRef}
              width={width}
              height={height}
              deferredMeasurementCache={cache}
              rowHeight={cache.rowHeight}
              rowRenderer={Row}
              rowCount={messages.length}
              className="customScrollBar"
            />
          )}
        </AutoSizer>
      </div>
      <InputBox newConversation={false} recipients={[recipient]} />
    </div>
  );
};

const isToday = (date: Date) => {
  const currentDate = new Date();

  if (
    date.getDate() === currentDate.getDate() &&
    date.getMonth() === currentDate.getMonth() &&
    date.getFullYear() === currentDate.getFullYear()
  )
    return true;

  return false;
};

const DateIndicator = ({ date }: { date: Date }) => {
  const { translations } = useTranslations();
  return (
    <div className={css.dateIndicator}>
      <span>
        {isToday(date) ? translations.today : date.toLocaleDateString()}
      </span>
    </div>
  );
};
