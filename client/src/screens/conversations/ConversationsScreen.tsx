import css from "./ConversationsScreen.module.scss";
import { Aside } from "./conversationsList/ConservationsList";
import { Outlet, Route, Routes } from "react-router-dom";
import { useUserContext } from "../../contexts/user";
import { NewConversation, ConversationBody } from "./chat/Chat";

const ConversationsScreen = () => {
  const userContext = useUserContext();

  return (
    <div className={css.main}>
      <Aside userId={userContext.userData._id} />

      <Routes>
        <Route index element={<ConversationBody />} />
        <Route path="/new/:uid/:username" element={<NewConversation />} />
      </Routes>

      <Outlet />
    </div>
  );
};

export default ConversationsScreen;
