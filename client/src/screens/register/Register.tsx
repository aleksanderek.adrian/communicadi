import React from 'react';
import css from './Register.module.scss'
import { Link } from 'react-router-dom';
import {LoadingModal} from '../../components/loading/LoadingSpinner';
import FormInput, { ConfirmPassword } from '../../components/input/FormInput';
import { useUserContext } from '../../contexts/user';
import { useRequest } from '../../hooks/useRequest';
import { useTranslations } from '../../contexts/translations';

const URL = process.env.REACT_APP_API_URL

const Logo = () => {

    return (
        <h2 className={css.logo}>{`>Communicadi`}</h2>
    )
}


const Register = () => {
    const {translations} = useTranslations()
    const userContext = useUserContext()
    const {sendRequest, isLoading, error} = useRequest()

    const passwordInputRef = React.useRef<HTMLInputElement>(null)
    const [password, setPassword] = React.useState('')

    React.useEffect(() => {
        const current = passwordInputRef?.current
        if(current) {
            const listener = (e: Event) => {
                setPassword(current.value)
            }
            current.addEventListener("change", listener)

            return () => current.removeEventListener("change", listener)
        }
    }, [passwordInputRef])


    const register = async (username: string, password: string) => {
        try {

            const responseData = await sendRequest(
                `${URL}/api/user/register`,
                'POST',
                  JSON.stringify({
                    username,
                    password
                }),
                {
                  'Content-Type': 'application/json'
                }
              )
              if(responseData) {
                userContext.login({_id: responseData._id, username: responseData.username}, responseData.token, responseData.key)
    
              }
        }
        catch {}
    }


    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        const form = e.currentTarget
        form.classList.add(css.submitet)
        const isValid = form.checkValidity()
        if(isValid) {
            const username = (form.elements.namedItem('username') as HTMLInputElement).value
            const password = (form.elements.namedItem('password') as HTMLInputElement).value
            await register(username, password)
        }
    }

    return (
        <div className={css.main}>
            <form noValidate className={css.box} onSubmit={handleSubmit}>
                <Logo/>
                { isLoading && <LoadingModal/>}
                <h3>{translations.signingUp}</h3>
                <div className={css.boxInputs}>
                    <FormInput name="username" label={translations.username} type='text' required minLength={3}/>
                    <FormInput ref={passwordInputRef} name="password" label={translations.password} type='password' required minLength={8}/>
                    <ConfirmPassword  label={translations.confirmPassword} pattern={password}/>
                </div>
                <p className={css.serverError}>{error}</p>
                <div className={css.boxBottom}>
                    <div>
                        <p>{translations.hasAccount}</p>
                        <Link to={`/`}>{translations.singIn}</Link>
                    </div>
                    <button type="submit">
                        {translations.signUp}
                    </button>
                </div>
            </form>
        </div>
    );
}

export default Register;