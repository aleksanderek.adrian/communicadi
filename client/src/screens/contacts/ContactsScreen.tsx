import css from "./ContactsScreen.module.scss";
import ContactsList from "./contactsList/ContactsList";
import ContactsFinder from "./contactsFinder/ContactsFinder";
import { useUserContext } from "../../contexts/user";
import { useContactsContext } from "../../contexts/contacts";

const ContactsScreen = () => {
  const userContext = useUserContext();
  const contactsContext = useContactsContext();

  return (
    <div className={css.main}>
      <ContactsList
        contactsContext={contactsContext}
        userId={userContext.userData._id}
      />
      <ContactsFinder />
    </div>
  );
};

export default ContactsScreen;
