import React from "react";
import css from "./ContactsFinder.module.scss";
import { ReactComponent as SearchIcon } from "../../../assets/search.svg";
import { ReactComponent as DeleteIcon } from "../../../assets/delete.svg";

import { LoadingSpinner } from "../../../components/loading/LoadingSpinner";
import { useRequest } from "../../../hooks/useRequest";
import { User, useUserContext } from "../../../contexts/user";
import { useConversationContext } from "../../../contexts/conversations";
import { useModalContext } from "../../../contexts/modal";
import { Tooltip } from "react-tooltip";
import { Translations, useTranslations } from "../../../contexts/translations";

const URL = process.env.REACT_APP_API_URL;

interface AddContactProps {
  onSearch: () => Promise<void>;
  query: string;
  setQuery: React.Dispatch<React.SetStateAction<string>>;
}

const SearchBox = (props: AddContactProps) => {
  const { translations } = useTranslations();
  const onEnterHandler = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === "Enter") {
      e.preventDefault();
      props.onSearch();
    }
  };

  const onClickHandler = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.preventDefault();
    props.onSearch();
  };

  return (
    <div className={css.addContact}>
      <h2>{translations.addNewContact}</h2>
      <p>{translations.searchUsersByUsername}</p>
      <form className={css.inputWrapper}>
        <input
          placeholder={translations.searchUsers}
          onKeyDown={onEnterHandler}
          value={props.query}
          type="text"
          onChange={(e) => props.setQuery(e.currentTarget.value)}
        />
        <button
          data-tooltip-id="search-tooltip"
          data-tooltip-content={translations.search}
          className={css.searchButton}
          onClick={onClickHandler}
        >
          <Tooltip className="tooltip" delayShow={500} id="search-tooltip" />

          <SearchIcon className={css.searchIcon} />
        </button>
      </form>
    </div>
  );
};

interface FoundUserItemProps {
  user: FoundUserI;
  addContact: (user: User) => void;
  goToConversation: (recipient: User) => void;
  translations: Translations;
}

const FoundUserItem = (props: FoundUserItemProps) => {
  const { username, isAlreadyAdded } = props.user;

  const [open, setOpen] = React.useState(false);

  const onAddToContacts = () => {
    setOpen(false);
    props.addContact(props.user);
  };

  const onClickHandler = () => {
    if (!isAlreadyAdded) {
      setOpen(!open);
    }
  };

  const onGoToConversation = () => {
    props.goToConversation(props.user);
  };

  return (
    <li className={isAlreadyAdded ? css.contactAdded : css.contact}>
      <div className={css.info} onClick={onClickHandler}>
        <span className={css.user}>{username}</span>
        {isAlreadyAdded && (
          <span
            className={css.added}
          >{`(${props.translations.alreadyAdded})`}</span>
        )}
      </div>
      <div className={`${css.agreement} ${open ? css.open : css.close}`}>
        <div>
          <button onClick={onAddToContacts}>
            {props.translations.addToContacts}
          </button>
          <button onClick={onGoToConversation}>
            {props.translations.sendDirectMessage}
          </button>
        </div>
      </div>
    </li>
  );
};

interface SearchResultProps {
  users: FoundUserI[];
  errorMessage: string;
  addContact: (user: User) => void;
  reset: () => void;
  goToConversation: (recipient: User) => void;
}

const ResetButton = ({ onClick }: { onClick: () => void }) => {
  const { translations } = useTranslations();

  return (
    <button
      onClick={onClick}
      data-tooltip-id="reset-tooltip"
      data-tooltip-content={translations.searchReset}
    >
      <Tooltip className="tooltip" delayShow={500} id="reset-tooltip" />
      <DeleteIcon />
    </button>
  );
};

const SearchResult = (props: SearchResultProps) => {
  const { translations } = useTranslations();

  const usersCount = props.users.length;

  const noResult = (
    <div className={css.noResult}>
      <div className={css.resultHeader}>
        <h2>{translations.noUsersFound}</h2>
        <ResetButton onClick={props.reset} />
      </div>
    </div>
  );

  const error = (
    <div className={css.noResult}>
      <h2>{props.errorMessage}</h2>
    </div>
  );

  const result = (
    <div className={css.found}>
      <div className={css.resultHeader}>
        <h2>{`${translations.usersFound}: ${usersCount}`}</h2>
        <ResetButton onClick={props.reset} />
      </div>

      <ul className={`customScrollBar ${css.list}`}>
        {props.users.map((user, index) => (
          <FoundUserItem
            key={`found-user-${index}`}
            translations={translations}
            goToConversation={props.goToConversation}
            addContact={props.addContact}
            user={user}
          />
        ))}
      </ul>
    </div>
  );

  return (
    <div className={css.result}>
      {props.errorMessage ? error : usersCount > 0 ? result : noResult}
    </div>
  );
};

export interface FoundUserI extends User {
  isAlreadyAdded: boolean;
}

const ContactsFinder = () => {
  const [submited, setSubmitet] = React.useState<boolean>(false);
  const [users, setUsers] = React.useState<FoundUserI[]>([]);
  const [searchQuery, setSearchQuery] = React.useState("");
  const conversationContext = useConversationContext();
  const modalContext = useModalContext();
  const userContext = useUserContext();

  const reset = () => {
    setUsers([]);
    setSearchQuery("");
    setSubmitet(false);
  };

  const { isLoading, sendRequest, error } = useRequest();

  const performSearch = React.useCallback(async () => {
    if (searchQuery.length > 0) {
      setSubmitet(true);
      try {
        const responseData: { users: FoundUserI[] } = await sendRequest(
          `${URL}/api/users/${searchQuery}/${userContext.userData._id}`,
          "GET",
          undefined,
          {
            Authorization: "Bearer " + userContext.token,
          }
        );
        setUsers(responseData.users);
      } catch {}
    }
  }, [searchQuery, userContext.token, userContext.userData._id, sendRequest]);

  const addContact = React.useCallback((user: User) => {
    modalContext.openAddContactModal(user);
    reset();
    // eslint-disable-next-line
  }, []);

  return (
    <div className={css.main}>
      <SearchBox
        query={searchQuery}
        setQuery={setSearchQuery}
        onSearch={performSearch}
      />
      {isLoading && (
        <div className={css.loading}>
          <LoadingSpinner />
        </div>
      )}
      {submited && !isLoading && (
        <SearchResult
          goToConversation={conversationContext.goToPrivateConversation}
          reset={reset}
          addContact={addContact}
          errorMessage={error ?? ""}
          users={users}
        />
      )}
    </div>
  );
};

export default ContactsFinder;
