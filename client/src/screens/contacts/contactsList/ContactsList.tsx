import React from "react";
import css from "./ContactsList.module.scss";
import SideList from "../../../components/sideList/SideList";
import { useConversationContext } from "../../../contexts/conversations";
import { User } from "../../../contexts/user";
import { Contact, ContactsContextType } from "../../../contexts/contacts";
import { useModalContext } from "../../../contexts/modal";
import Dropdown from "../../../components/dropdown/Dropdown";
import {Translations, useTranslations } from "../../../contexts/translations";

interface ContactItemsProps {
  contactData: Contact;
  openDeleteModal: (contact: Contact) => void;
  openEditModal: (contact: Contact) => void;
  translations: Translations;
  goToConversation: (recipient: User) => void;
}

const ContactItem = (props: ContactItemsProps) => {
  const isContactName = (props.contactData?.name ?? "").length > 0;

  const onClickHandler = () => {
    props.goToConversation(props.contactData);
  };

  const onDelete = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    e.stopPropagation();

    props.openDeleteModal(props.contactData);
  };

  const onEdit = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    e.stopPropagation();

    props.openEditModal(props.contactData);
  };

  return (
    <li className={`${css.contact}`} key={props.contactData._id}>
      <div className={css.wrapper}>
        <div className={css.userData} onClick={onClickHandler}>
          <div className={`${css.name} ${css.textOverflow}`}>
            {isContactName
              ? props.contactData.name
              : props.contactData.username}
          </div>
          {isContactName && (
            <div className={`${css.user} ${css.textOverflow}`}>
              {props.contactData.username}
            </div>
          )}
        </div>
        <div className={css.icons}>
          <Dropdown>
            <button onClick={onEdit}>{props.translations.edit}</button>
            <button onClick={onDelete}>{props.translations.delete}</button>
          </Dropdown>
        </div>
      </div>
    </li>
  );
};

interface ContactsListProps {
  userId: string;
  contactsContext: ContactsContextType;
}

const ContactsList = ({ contactsContext }: ContactsListProps) => {
  const { translations } = useTranslations();
  const conversationsContext = useConversationContext();
  const modalContext = useModalContext();

  const contacts = contactsContext.getContacts();

  const openContactDeleteModal = (contact: Contact) => {
    modalContext.openDeleteContactModal(contact);
  };

  const openEditModal = (contact: Contact) => {
    modalContext.openEditContactModal(contact);
  };

  const [searchText, setSearchText] = React.useState("");

  const concatName = (contact: Contact) => {
    return contact.name + contact.username;
  };

  const filtered = contacts.filter(
    (c) => concatName(c).indexOf(searchText) !== -1
  );
  return (
    <div className={css.main}>
      <div className={css.contactsList}>
        <div className={css.header}>
          <input
            placeholder={translations.searchContacts}
            onChange={(e) => setSearchText(e.currentTarget.value)}
            type="text"
            className={css.conversationSearch}
          />
        </div>
        {filtered.length === 0 ? (
          <div className={css.noContacts}>{translations.noContacts}</div>
        ) : (
          <SideList>
            {filtered.map((contact, index) => (
              <ContactItem
                key={`contact-item-${index}`}
                translations={translations}
                openEditModal={openEditModal}
                goToConversation={conversationsContext.goToPrivateConversation}
                openDeleteModal={openContactDeleteModal}
                contactData={contact}
              />
            ))}
          </SideList>
        )}
      </div>
    </div>
  );
};

export default ContactsList;
