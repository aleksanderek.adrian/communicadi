const addLeadingZero = (val: number) => {
  if (val.toString().length === 1) {
    return "0" + val;
  }
  return val.toString();
};

export const getTime = (date: Date | undefined, displaySeconds = true) => {
  if (date) {
    const hours = addLeadingZero(date.getHours());
    const minutes = addLeadingZero(date.getMinutes());
    const seconds = addLeadingZero(date.getSeconds());

    if (displaySeconds) {
      return `${hours}:${minutes}:${seconds}`;
    } else {
      return `${hours}:${minutes}`;
    }
  }
};
