import React from "react";
import css from "./Modal.module.scss";
import { LoadingSpinner } from "../loading/LoadingSpinner";

interface ModalProps {
  onClose?: () => void;
  isOpen: boolean;
  loading?: boolean;
}

export const Modal = (props: React.PropsWithChildren<ModalProps>) => {
  const [isModalOpen, setModalOpen] = React.useState(props.isOpen);
  const modalRef = React.useRef<HTMLDialogElement | null>(null);

  React.useEffect(() => {
    setModalOpen(props.isOpen);
  }, [props.isOpen]);

  React.useEffect(() => {
    const modalElement = modalRef.current;

    if (modalElement) {
      if (isModalOpen) {
        modalElement.showModal();
      } else {
        modalElement.close();
      }
    }
  }, [isModalOpen, modalRef]);

  React.useEffect(() => {
    const modalElement = modalRef.current;

    if (modalElement) {
      const listener = (e: Event) => {
        e.preventDefault();
      };
      modalElement.addEventListener("cancel", listener);

      return () => modalElement.removeEventListener("cancel", listener);
    }
  }, [modalRef]);

  return (
    <dialog
      ref={modalRef}
      className={`${css.modal} ${props.loading ? css.loadingModal : ""}`}
    >
      {props.loading ? <LoadingSpinner /> : props.children}
    </dialog>
  );
};
