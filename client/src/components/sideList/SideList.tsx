import React from "react";
import css from "./SideList.module.scss";

const SideList = (props: React.PropsWithChildren) => {
  return (
    <div className={css.listWrapper}>
      <ul className={`${css.list} customScrollBar`}>{props.children}</ul>
      <div className={css.mask}>
        <div></div>
      </div>
    </div>
  );
};

export default SideList;
