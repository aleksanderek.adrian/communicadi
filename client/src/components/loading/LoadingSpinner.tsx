import css from "./LoadingSpinner.module.scss";
import ReactDOM from "react-dom";

interface LoadingSpinnerProps {
  small?: boolean;
}

export const LoadingSpinner = ({ small }: LoadingSpinnerProps) => {
  return (
    <div className={css.wrapper}>
      <div
        className={small ? css.loadingSpinnerSmall : css.loadingSpinner}
      ></div>
    </div>
  );
};

export const LoadingModal = () => {
  const modal = (
    <div className={css.modal}>
      <div className={css.loadingSpinner}></div>
    </div>
  );

  return ReactDOM.createPortal(modal, document.body);
};
