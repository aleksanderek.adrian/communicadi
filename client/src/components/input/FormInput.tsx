import React from "react";
import css from "./FormInput.module.scss";
import { useTranslations } from "../../contexts/translations";

interface InputProps extends React.ComponentPropsWithoutRef<"input"> {
  label: string;
  onChangeCallback?: () => void;
}

const FormInput = React.forwardRef<HTMLInputElement, InputProps>(
  ({ label, onChangeCallback, ...rest }, ref) => {
    const [error, setError] = React.useState<string>("");

    const onInvalid = (e: React.FormEvent<HTMLInputElement>) => {
      const el = e.target as HTMLInputElement;
      setError(el.validationMessage);
      el.classList.add(css.inputInvalid);
    };

    const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
      const el = e.target as HTMLInputElement;
      if (error) {
        setError(el.validationMessage);
      }
      if (!el.validationMessage) {
        el.classList.remove(css.inputInvalid);
      }
    };
    return (
      <div className={css.inputWrapper}>
        <label>{label}</label>
        <input ref={ref} onInvalid={onInvalid} onChange={onChange} {...rest} />

        <p className={css.error}>{error}</p>
      </div>
    );
  }
);

export const ConfirmPassword = ({
  label,
  pattern,
}: {
  label: string;
  pattern: string;
}) => {
  const { translations } = useTranslations();

  const [error, setError] = React.useState<string>("");

  const onInvalid = (e: React.FormEvent<HTMLInputElement>) => {
    const el = e.target as HTMLInputElement;
    el.setCustomValidity(translations.passwordDontMatch);
    setError(el.validationMessage);
    el.classList.add(css.inputInvalid);
  };

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const el = e.target as HTMLInputElement;
    if (el.value !== pattern) {
      el.setCustomValidity(translations.passwordDontMatch);
      setError(el.validationMessage);
      el.classList.add(css.inputInvalid);
    } else {
      el.setCustomValidity("");
      setError("");
      el.classList.remove(css.inputInvalid);
    }
  };

  return (
    <div className={css.inputWrapper}>
      <label>{label}</label>
      <input
        type="password"
        onChange={onChange}
        onInvalid={onInvalid}
        required
        minLength={8}
      />

      <p className={css.error}>{error}</p>
    </div>
  );
};

export default FormInput;
