import React from "react";
import css from "./Dropdown.module.scss";
import { ReactComponent as OptionsDots } from "../../assets/options-dots.svg";

const Dropdown = (props: React.PropsWithChildren) => {
  const [open, setOpen] = React.useState(false);

  const dropdownRef = React.useRef<HTMLDivElement>(null);

  const onClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    e.stopPropagation();
    setOpen(!open);
  };

  React.useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (
        dropdownRef.current &&
        !dropdownRef.current.contains(event.target as Node)
      ) {
        setOpen(false);
      }
    };

    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  return (
    <div ref={dropdownRef} className={css.main}>
      <button onClick={onClick}>
        <OptionsDots className={`${css.dots} ${open ? css.opened : ""}`} />
      </button>

      <div className={css.dropdown} style={{ display: open ? "flex" : "none" }}>
        {props.children}
      </div>
    </div>
  );
};

export default Dropdown;
