import css from "./Header.module.scss";
import { matchPath, useLocation, useNavigate } from "react-router-dom";
import { ReactComponent as ExitIcon } from "../../assets/exit.svg";
import { useUserContext } from "../../contexts/user";
import { useConversationContext } from "../../contexts/conversations";
import { ReactComponent as DeleteIcon } from "../../assets/delete.svg";
import { useContactsContext } from "../../contexts/contacts";
import { Tooltip } from "react-tooltip";
import { useSocket } from "../../contexts/socket";
import { useTranslations } from "../../contexts/translations";

const ConnectionIndicator = () => {
  const { connected } = useSocket();

  return (
    <div
      className={`${css.connectionIndicator} ${
        connected ? css.online : css.offline
      }`}
    ></div>
  );
};

const Header = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const currentRoute = location.pathname;
  const { translations } = useTranslations();

  const userContext = useUserContext();
  const conversationContext = useConversationContext();
  const contactsContext = useContactsContext();

  const isConversationsPath = matchPath(
    { path: "/dashboard/conversations/*" },
    currentRoute
  );
  const isContactsPath = matchPath(
    { path: "/dashboard/contacts/*" },
    currentRoute
  );

  const notification = conversationContext.notification;
  let contact;
  if (notification) {
    contact = contactsContext.getContact(notification.sender._id);
  }

  const onNotificationClick = () => {
    if (notification) {
      conversationContext.clearNotification();
      conversationContext.goToPrivateConversation(notification.sender);
    }
  };

  return (
    <div className={css.main}>
      <div className={css.userBar}>
        <div className={css.userNameWrapper}>
          <p className={css.userName}>{userContext.userData.username}</p>
          <ConnectionIndicator />
        </div>
        <button
          data-tooltip-id="exit-tooltip"
          data-tooltip-content={translations.logout}
          onClick={() => userContext.logout()}
          className={css.logout}
        >
          <ExitIcon />
          <Tooltip className="tooltip" delayShow={500} id="exit-tooltip" />
        </button>
      </div>
      <div className={css.bottomBar}>
        <div className={css.pages}>
          <button
            onClick={() => navigate("/dashboard/conversations")}
            className={`${css.page} ${
              isConversationsPath ? css.pageActive : ""
            }`}
          >
            {translations.conversations}
          </button>
          <button
            onClick={() => navigate("/dashboard/contacts")}
            className={`${css.page} ${isContactsPath ? css.pageActive : ""}`}
          >
            {translations.contacts}
          </button>
        </div>
        <div className={css.notificationWrapper}>
          {notification && (
            <div className={css.notification}>
              <div onClick={onNotificationClick}>{`${
                translations.newMessageFrom
              } ${
                contact?.name
                  ? contact.name + " | " + notification.sender.username
                  : notification.sender.username
              }`}</div>
              <DeleteIcon
                className={css.notificationDelete}
                onClick={() => conversationContext.clearNotification()}
              />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Header;
