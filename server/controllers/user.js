const userModel = require("../models/users").userModel;
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const dotenv = require("dotenv");

dotenv.config();

const secret = process.env.SECRET;

const signUp = async (req, res, next) => {
  const { username, password } = req.body;

  let existingUser;
  try {
    existingUser = await userModel
      .findOne({ username })
      .collation({ locale: "pl", strength: 2 })
      .exec();

    if (existingUser) {
      return res.status(409).json({ message: "User already exist." });
    }

    let hashedPassword = await bcrypt.hash(password, 12);

    const newUser = new userModel({
      username,
      password: hashedPassword,
      conversations: [],
      contacts: [],
      inbox: [],
    });

    await newUser.save();

    let token = jwt.sign({ userId: newUser.id }, secret);

    res.status(201).json({
      _id: newUser.id,
      username: newUser.username,
      token,
      key: process.env.KEY,
    });
  } catch {
    return res
      .status(400)
      .json({ message: "Something went wrong. Try again later." });
  }
};

const login = async (req, res, next) => {
  const { username, password } = req.body;

  let existingUser;
  try {
    existingUser = await userModel
      .findOne({ username })
      .collation({ locale: "pl", strength: 2 })
      .exec();

    if (!existingUser) {
      return res.status(400).json({ message: "Check credentials." });
    }

    let isValidPassword = await bcrypt.compare(password, existingUser.password);

    if (!isValidPassword) {
      return res.status(400).json({ message: "Check credentials." });
    }

    let token = jwt.sign({ userId: existingUser.id }, secret);

    res.json({
      _id: existingUser.id,
      username: existingUser.username,
      token,
      key: process.env.KEY,
    });
  } catch (err) {

    return res
      .status(400)
      .json({ message: "Something went wrong. Try again later." });
  }
};

const addToInbox = async (userId, message) => {
  try {
    await userModel.findByIdAndUpdate(userId, {
      $push: {
        inbox: message,
      },
    });
  } catch {}
};

const addContant = async (req, res, next) => {
  const { userId, contactId, name } = req.body;
  let user;
  let contact;

  try {
    user = await userModel.findById(userId);
    contact = await userModel.findById(contactId);

    if (user && contact) {
      const isContactAdded = user.contacts.find((contact) =>
        contact._id.equals(contactId)
      );
      if (isContactAdded) {
        return res.status(400).json({ message: "Contact already exists." });
      }

      if (name) {
        user.contacts.push({
          _id: contact.id,
          username: contact.username,
          name,
        });
      } else {
        user.contacts.push({
          _id: contact.id,
          username: contact.username,
        });
      }

      await user.save();

      res.status(200).json({
        _id: contact.id,
        username: contact.username,
        name,
      });
    }
  } catch {
    return res
      .status(400)
      .json({ message: "Something went wrong. Try again later." });
  }
};

const editContact = async (req, res, next) => {
  const { userId, contactId, name } = req.body;
  let user;

  try {
    user = await userModel.findById(userId);

    if (user) {
      const updatedContacts = user.contacts.map((c) => {
        if (c._id.equals(contactId)) {
          return {
            ...c,
            name,
          };
        } else {
          return c;
        }
      });
      user.contacts = updatedContacts;

      await user.save();
      res.status(200).json({ message: "Contact edited." });
    }
  } catch (err) {
    return res
      .status(400)
      .json({ message: "Something went wrong. Try again later." });
  }
};

const getContacts = async (req, res, next) => {
  const userId = req.params.uid;
  let user;

  try {
    user = await userModel.findById(userId);
    res.status(200).json({ contacts: user.contacts });
  } catch {
    return res.status(404).json({ message: "No user found." });
  }
};

const deleteContact = async (req, res, next) => {
  const userId = req.params.uid;
  const contactId = req.params.contactId;

  try {
    await userModel.findByIdAndUpdate(userId, {
      $pull: {
        contacts: { _id: new mongoose.Types.ObjectId(contactId) },
      },
    });
    res.status(200).json({ message: "Contact deleted." });
  } catch (err) {
    return res.status(404).json({ message: "No user found." });
  }
};

const getConversations = async (req, res, next) => {
  const userId = req.params.uid;

  let user;

  try {
    user = await userModel.findById(userId);
    res.status(200).json({ conversations: user.conversations });
  } catch (err) {
    return res.status(404).json({ message: "No user found." });
  }
};

const getInbox = async (req, res, next) => {
  const userId = req.params.uid;
  let user;

  try {
    user = await userModel.findById(userId);
    res.status(200).json({ messages: user.inbox });
  } catch (err) {
    return res.status(404).json({ message: "No user found." });
  }
};

const clearInbox = async (req, res, next) => {
  const userId = req.params.uid;
  let user;

  try {
    user = await userModel.findById(userId);
    user.inbox = [];
    await user.save();
    res.status(200).json({ message: "Inbox cleared." });
  } catch (err) {
    return res.status(404).json({ message: "No user found." });
  }
};

const deleteConversation = async (req, res, next) => {
  const userId = req.params.uid;
  const convId = req.params.conversationId;

  try {
    await userModel.findByIdAndUpdate(userId, {
      $pull: {
        conversations: { id: convId },
      },
    });
    res.status(200).json({ message: "Contact deleted." });
  } catch (err) {
    return res.status(404).json({ message: "No user found." });
  }
};

const storeMessage = async (req, res, next) => {
  const { message, conversationId } = req.body;
  const userId = req.params.uid;
  let user;
  let conversationIndex;

  try {
    user = await userModel.findById(userId);
    const participants = [message.sender].concat(message.recipients);
    conversationIndex = user.conversations.findIndex(
      (conv) => conv.id === conversationId
    );

    if (conversationIndex >= 0) {
      user.conversations[conversationIndex].messages.push(message);
      await user.save();
    } else {
      const newConversation = {
        id: conversationId,
        participants,
        messages: [message],
      };
      user.conversations.push(newConversation);
      await user.save();
    }
    res.status(200).json({ message: "Message stored." });
  } catch (err) {
    return res
      .status(400)
      .json({ message: "Something went wrong. Try again later." });
  }
};

const alterMessage = async (req, res, next) => {
  const { message, conversationId } = req.body;
  const userId = req.params.uid;
  let user;
  let conversationIndex;

  try {
    user = await userModel.findById(userId);

    conversationIndex = user.conversations.findIndex(
      (conv) => conv.id === conversationId
    );

    if (conversationIndex >= 0) {
      const messageIndex = user.conversations[
        conversationIndex
      ].messages.findIndex((msg) => msg.id === message.id);
      if (messageIndex >= 0) {
        user.conversations[conversationIndex].messages[messageIndex] = message;
        await user.save();
      }
    }

    res.status(200).json({ message: "Message altered." });
  } catch (err) {
    return res
      .status(400)
      .json({ message: "Something went wrong. Try again later." });
  }
};

exports.signUp = signUp;
exports.login = login;
exports.addContant = addContant;
exports.getContacts = getContacts;
exports.deleteContact = deleteContact;
exports.getConversations = getConversations;
exports.addToInbox = addToInbox;
exports.getInbox = getInbox;
exports.clearInbox = clearInbox;
exports.storeMessage = storeMessage;
exports.alterMessage = alterMessage;
exports.deleteConversation = deleteConversation;
exports.editContact = editContact;
