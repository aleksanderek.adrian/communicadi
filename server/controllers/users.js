const User = require("../models/users").userModel;

const findUsers = async (req, res, next) => {
  const result = [];
  let foundContacts;
  let user;

  const query = req.params.query;
  const userId = req.params.uid;

  try {
    foundContacts = await User.find({
      username: { $regex: query, $options: "i" },
    });
    user = await User.findById(userId);

    foundContacts.map((foundContact) => {
      if (!foundContact._id.equals(userId)) {
        result.push({
          _id: foundContact.id,
          username: foundContact.username,
          isAlreadyAdded:
            user.contacts.find((contact) =>
              contact._id.equals(foundContact.id)
            ) !== undefined,
        });
      }
    });

    res.json({ users: result });
  } catch (err) {
    return res
      .status(400)
      .json({ message: "Something went wrong. Try again later." });
  }
};

exports.findUsers = findUsers;
