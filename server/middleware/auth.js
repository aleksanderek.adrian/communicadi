const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");

dotenv.config();

const secret = process.env.SECRET;

module.exports = (req, res, next) => {
  if (req.method === "OPTIONS") {
    return next();
  }

  try {
    const authorizationHeader = req.headers.authorization;

    if (!authorizationHeader || !authorizationHeader.startsWith("Bearer ")) {
      throw new Error();
    }

    const token = authorizationHeader.split(" ")[1]; // Get the token

    if (!token) {
      throw new Error();
    }

    const decodedToken = jwt.verify(token, secret);
    req.userData = { userId: decodedToken.userId };
    next();
  } catch (err) {
    return res.status(401).json({ message: "Authentication failed!" });
  }
};
