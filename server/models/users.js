const mongoose = require("mongoose");
const Conversation = require("./conversation");
const Schema = mongoose.Schema;

const contactSchema = new Schema({
  username: { type: String, required: true, minlength: 3 },
  name: { type: String, minlength: 1 },
});

const userSchema = new Schema({
  username: { type: String, required: true, unique: true, minlength: 3 },
  password: { type: String, required: true, minlength: 8 },
  conversations: [{ type: Conversation.conversationSchema, required: true }],
  contacts: { type: [contactSchema], required: true },
  inbox: { type: [Conversation.messageSchema], required: true },
});

exports.userModel = mongoose.model("User", userSchema);
