const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userPartialSchema = new Schema({
  username: { type: String, required: true, minlength: 3 },
});

const messageSchema = new Schema(
  {
    id: { type: String, required: true },
    sender: { type: userPartialSchema, required: true },
    recipients: { type: [userPartialSchema], required: true },
    content: { type: String, required: true },
    sentAt: { type: Date, required: true },
    createdAt: { type: Date, required: true },
    status: { type: String, required: true },
  },
  {
    autoCreate: false,
  }
);

const conversationSchema = new Schema({
  id: { type: String, required: true },
  participants: { type: [userPartialSchema], required: true },
  messages: [messageSchema],
});

exports.messageSchema = messageSchema;
exports.conversationSchema = conversationSchema;
