const dotenv = require("dotenv");
const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/user");
const userController = require("./controllers/user");
const usersRoutes = require("./routes/users");
const http = require("http");
const bodyParser = require("body-parser");

dotenv.config();

const port = process.env.PORT;
const uri = process.env.URI;
const app = express();
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server, { cors: { origin: "*" } });

console.log("SERVER IS STARTING");

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, DELETE, PUT"
  );

  next();
});

mongoose
  .connect(uri)
  .then(() => {
    io.on("connection", (socket) => {
      const userId = socket.handshake.query.userId;
      socket.join(userId);

      socket.on("send-message", async ({ message, conversationId }) => {
        const recipients = message.recipients;

        const sentMessage = {
          id: message.id,
          sender: message.sender,
          recipients: message.recipients,
          content: message.content,
          sentAt: new Date(),
          createdAt: message.createdAt,
          status: "sent",
        };

        await Promise.all(
          recipients.map(async (recipient) => {
            const isRoomExisting = io.sockets.adapter.rooms.get(recipient._id);

            if (isRoomExisting) {
              // Send message to recipient if is online
              socket.broadcast
                .to(recipient._id)
                .emit("receive-message", { message: sentMessage });
            } else {
              // Add to recipient inbox if is offline
              await userController.addToInbox(recipient._id, sentMessage);
            }
          })
        );

        io.to(message.sender._id).emit("message-sent", {
          message: sentMessage,
          conversationId,
        });
      });
    });

    app.use(bodyParser.json());

    app.use("/api/user", userRoutes);

    app.use("/api/users", usersRoutes);

    server.listen(parseInt(port), () => {
      console.log("SERVER IS LISTENING ON PORT " + port);
    });
  })
  .catch(() => {
    console.log("DATABASE CONNECTION ERROR");
  });
