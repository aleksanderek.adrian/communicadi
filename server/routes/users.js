const express = require("express");
const usersControllers = require("../controllers/users");
const router = express.Router();
const auth = require("../middleware/auth");

router.use(auth);
router.get("/:query/:uid", usersControllers.findUsers);

module.exports = router;
