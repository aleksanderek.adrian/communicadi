const express = require("express");
const usersControllers = require("../controllers/user");
const auth = require("../middleware/auth");

const router = express.Router();

router.post("/register", usersControllers.signUp);
router.post("/login", usersControllers.login);

router.use(auth);

router.put("/contacts/add", usersControllers.addContant);
router.patch("/contacts/edit", usersControllers.editContact);
router.get("/contacts/:uid", usersControllers.getContacts);
router.delete(
  "/contacts/delete/:uid/:contactId",
  usersControllers.deleteContact
);
router.delete(
  "/conversations/delete/:uid/:conversationId",
  usersControllers.deleteConversation
);
router.get("/conversations/:uid", usersControllers.getConversations);
router.get("/conversations/inbox/:uid", usersControllers.getInbox);
router.delete("/conversations/inbox/clear/:uid", usersControllers.clearInbox);
router.put("/conversations/storeMessage/:uid", usersControllers.storeMessage);
router.put("/conversations/alterMessage/:uid", usersControllers.alterMessage);

module.exports = router;
