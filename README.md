# Communicadi - text messenger app

<p align="center">
  <img src="logo.png" alt="Logo">
</p>

A simple desktop chat application developed in electron, react, nodejs and mongodb. Supports direct messages only. Messages are encrypted using AES.

![Preview image](preview.png)

## Technologies

- TypeScript 4.9.5
- React.js 18.2.0
- Electron.js 28.0.0
- Node.js 21.4.0
- MongoDb 7.0

## Setup

MongoDb server is required. By default, the database server address is set to *localhost:27017*. This can be changed in the server .env file.

### Node.js server

Install and run the server using npm:

```
cd server
npm install
npm start
```

Server should run on port 5000.

### Frontend

Install using npm:

```
cd client
npm install
```

To run app in dev mode:

```npm start```

It is also possible to run application in browser on [http://localhost:3000](http://localhost:3000) using:

```npm run dev```

To build electron application:

```npm run electron:build```

The application will be built in the dist folder.


